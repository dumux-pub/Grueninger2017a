#!/usr/bin/env bash

# We provide this script to download and patch all Dune modules for Grueninger2017a
#
# How to procede:
# 1. create an emtpy directory
# 2. copy this file into directory and execute it,
#    maybe you have to set the executable bit first
# 3. check the README.md to get the location of the executables

###
# download Dune modules
###
echo "###"
echo "# download Dune modules"
echo "###"

# this module
# must be checkout first to provide patches
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Grueninger2017a.git

# dune-common
# tags/v2.4.2 # 23e568987d8f24d2b188cd664a9fe51ccdc1393f # 2017-06-28 23:33:33 +0200 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout tags/v2.4.2
cd ..

# dune-geometry
# tags/v2.4.2 # 4330ebc930121949ce5e5b3918e26f5d4c8922a1 # 2017-06-28 23:34:01 +0200 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout tags/v2.4.2
cd ..

# dune-grid
# tags/v2.4.2 # 6d74754d667f093979f19687b1c76450461925bd # 2017-06-28 23:34:28 +0200 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout tags/v2.4.2
patch -p1 < ../Grueninger2017a/patches/dune-grid_9999-uncommitted-changes.patch
cd ..

# dune-localfunctions
# tags/v2.4.2 # cf91cc815324f24a3abe3356a55aa7a92b6f65e3 # 2017-06-28 23:35:40 +0200 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout tags/v2.4.2
cd ..

# dune-istl
# tags/v2.4.2 # c29b037dc2ae794d0810c532aa8ff6a493032970 # 2017-06-28 23:35:02 +0200 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout tags/v2.4.2
cd ..

# dune-typetree
# releases/2.3 # ecffa10c59fa61a0071e7c788899464b0268719f # 2014-07-23 17:49:24 +0200 # Steffen Müthing
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
cd dune-typetree
git checkout releases/2.3
git reset --hard ecffa10c59fa61a0071e7c788899464b0268719f
cd ..

# dune-pdelab
# releases/2.0 # 19c782eea7232e94849617b20dfee8d9781eb4fb # 2016-09-07 07:09:49 +0000 # Christian Engwer
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
cd dune-pdelab
git checkout releases/2.0
git reset --hard 19c782eea7232e94849617b20dfee8d9781eb4fb
patch -p1 < ../Grueninger2017a/patches/dune-pdelab_9999-ignore-PETSc.patch
cd ..

# dune-multidomaingrid
# releases/2.3 # 3b829b7a130473749b8af2d402eaef1eff1071a7 # 2016-09-26 16:55:13 +0200 # Steffen Müthing
git clone git://github.com/smuething/dune-multidomaingrid.git
cd dune-multidomaingrid
git checkout releases/2.3
git reset --hard 3b829b7a130473749b8af2d402eaef1eff1071a7
cd ..

# dune-multidomain
# releases/2.0 # e3d52982dc9acca9bf13cd8f77bf0329c61b6327 # 2016-09-26 15:13:50 +0200 # Steffen Müthing
git clone git://github.com/smuething/dune-multidomain.git
cd dune-multidomain
git checkout releases/2.0
git reset --hard e3d52982dc9acca9bf13cd8f77bf0329c61b6327
cd ..

# dumux
# master # b90661a60d71e083c3c5bb4abe7b697eac949312 # 2017-03-17 09:29:57 +0100 # Sina Ackermann
git clone https://gruenich@git.iws.uni-stuttgart.de/dumux-repositories/dumux
cd dumux
git checkout master
git reset --hard efb753d4cc3c0e96aad8430ffb66ea0519b372c7
cd ..

###
# configure and build modules
###
echo "###"
echo "# configure and build modules"
echo "###"

./dune-common/bin/dunecontrol --opts=dumux/optim.opts all
