#!/usr/bin/env bash

# fill arrays with test names and paths
arrayTests=("test_evaporationpipe"
  "test_evaporationpipe3d"
  "test_evaporationpipesuperlu"
  "test_evaporationpipe3dsuperlu"
  "test_evaporationpipegmres"
  "test_evaporationpipe3dgmres"
  "test_evaporationpipere5000"
  "test_evaporationpipere2500"
  "test_evaporationpipere500"
  "test_evaporationpipere50"
  "test_evaporationpipere5"
  "test_evaporationpipere05"
  "test_channelfuelcell"
  "test_fuelcellgeometry"
  "test_ventilationgallery"
  [15]="test_ventilationgalleryconstlambda")

arrayInputs=("test_evaporationpipe.input"
  "test_simplepipeslice.input"
  "test_simplepipe2d.input"
  "test_simplepipeslice.input"
  "test_simplepipe2d.input"
  "test_simplepipeslice.input"
  "test_simplepipe2d.input"
  "test_simplepipe2d.input"
  "test_simplepipe2d.input"
  "test_simplepipe2d.input"
  "test_simplepipe2d.input"
  "test_simplepipe2d.input"
  "test_channelfuelcell.input"
  "test_fuelcellgeometry.input"
  "test_ventilationgallery.input"
  [15]="test_ventilationgallery.input")

# check argument passed, print help
if [[ $# -ne 1 || $1 == -h || $1 == --help ]]; then
  echo "Add one of the following names as an argument to execute the example you want to run:"
  for item in ${arrayTests[*]}
  do
    printf "* %s\n" $item
  done
  echo "Check the README.md for a brief explanation for the executables."
  exit
fi

# check for valid argument
if [[ $1 != ${arrayTests[0]}
      && $1 != ${arrayTests[1]}
      && $1 != ${arrayTests[2]}
      && $1 != ${arrayTests[3]}
      && $1 != ${arrayTests[4]}
      && $1 != ${arrayTests[5]}
      && $1 != ${arrayTests[6]}
      && $1 != ${arrayTests[7]}
      && $1 != ${arrayTests[8]}
      && $1 != ${arrayTests[9]}
      && $1 != ${arrayTests[10]}
      && $1 != ${arrayTests[11]}
      && $1 != ${arrayTests[12]}
      && $1 != ${arrayTests[13]}
      && $1 != ${arrayTests[14]}
      && $1 != ${arrayTests[15]} ]]; then
  echo "Unknown executable \"$1\". Use -h to get list of executables"
  exit
fi

# build and execute
for index in {0..15}
do
  if [ "$1" == ${arrayTests[$index]} ]; then
    cd build-cmake
    make ${arrayTests[$index]}
    cd ..
    ./build-cmake/appl/multidomain/promogruenich/${arrayTests[$index]} ./appl/multidomain/promogruenich/${arrayInputs[$index]}
    break
  fi
done
