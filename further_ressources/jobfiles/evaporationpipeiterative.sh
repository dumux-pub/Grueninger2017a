#!/bin/bash
umask 022

# check for valid arugument
if [ -z "$1" ]; then
  echo "No argument supplied, argument 1 must be the build directory"
  exit 2
fi

# custom input and output folder (adapt to your needs)
builddir=$1
outdir=$builddir/../results/

# predefined names
executable=test_evaporationpipegmres
input=test_evaporationpipe.input
sourcedir=$builddir/appl/multidomain/promogruenich/

# make executable
cd $sourcedir
make $executable

# refinement level: 0
echo ""
echo "refinement level: 0"
echo ""
simdir=$outdir/evaporationpipe/iterativerefine0

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 '18 12 5' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 1
echo ""
echo "refinement level: 1"
echo ""
simdir=$outdir/evaporationpipe/iterativerefine1

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 '36 24 10' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 2
echo ""
echo "refinement level: 2"
echo ""
simdir=$outdir/evaporationpipe/iterativerefine2

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 '72 48 20' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 3
echo ""
echo "refinement level: 3"
echo ""
simdir=$outdir/evaporationpipe/iterativerefine3

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 '144 96 40' \
  -TimeManager.DtInitial 0.01
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# # refinement level: 4
# echo ""
# echo "refinement level: 4"
# echo ""
# simdir=$outdir/evaporationpipe/iterativerefine4
#
# # create output folder and copy all necessary files
# if [ -e $simdir ]; then
#   echo "Output folder already exists, aborting"
#   exit 1
# fi
# mkdir -p $simdir
#
# cp $sourcedir/$executable $simdir
# cp $sourcedir/$input $simdir
# cd $simdir
#
# echo "simulation starts on $HOST" | tee logfile.out
# COMMAND="./$executable $input \
#   -Grid.Cells0 '288 192 80' \
#   -TimeManager.DtInitial 1e-5
#   | tee -a logfile.out"
# echo $COMMAND > simulation.sh && chmod u+x simulation.sh
# ./simulation.sh
# echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

exit 0
