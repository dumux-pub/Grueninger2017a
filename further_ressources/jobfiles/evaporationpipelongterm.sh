#!/bin/bash
umask 022

# simulate for 60 days to have complete drying

# check for valid arugument
if [ -z "$1" ]; then
  echo "No argument supplied, argument 1 must be the build directory"
  exit 2
fi

# custom input and output folder (adapt to your needs)
builddir=$1
outdir=$builddir/../results/

# predefined names
executable=test_evaporationpipe
input=test_evaporationpipe.input
sourcedir=$builddir/appl/multidomain/promogruenich/
simdir=$outdir/evaporationpipe/longterm

# make executable
cd $sourcedir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -FreeFlow.Velocity 0.247 \
  -TimeManager.TEnd 8640000 \
  -TimeManager.MaxTimeStepSize 900 \
  -Vtk.FreqOutput 100 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
