Summary
=======
This is the Dune module containing the code for reproducing the results
from the Ph.D. thesis:

*Numerical Coupling of Navier-Stokes and Darcy Flow for Soil-Water Evaporation.  
C. Grüninger, University of Stuttgart, May 2017.  
[doi: 10.18419/opus-???](http://dx.doi.org/10.18419/opus-???)*

[BibTeX entry](grueninger2017a.bib)


Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and to execute the file
[installGrueninger2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Grueninger2017a/raw/master/installGrueninger2017a.sh)
in this directory.

```bash
mkdir -p Grueninger2017a && cd Grueninger2017a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Grueninger2017a/raw/master/installGrueninger2017a.sh
chmod u+x installGrueninger2017a.sh
./installGrueninger2017a.sh
```

You need to have installed at least:
* CMake 2.8.12
* C, C++ compiler (GCC 4.8 or Clang 3.5 should be enough, the newer the better)
* Fortran compiler (gfortran)
* SuperLU or UMFPack from SuiteSparse
* Boost

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The applications used for this publication can be found in `appl/multidomain/promogruenich`.
There are tests which can be run using CTest to check whether the applications
can be executed, and for `test_evaporationpipere2500` the result is compared to
a reference result. To run the tests, compile all applications and run CTest
from the build directory
```bash
make build_tests
ctest

To compile the applications using GMRES and MC64, indicated by the application name
ending with `gmres`, you have to adjust
`appl/staggeredgrid/multidomain/common/iterativesolve.hh`, line 7 from
`/home/lh2/gruenich/downloads/superlu/CBLAS/daxpy.c`
to match the actual location of this CBLAS file on your system. Further, use a vanilla
SuperLU from their project website, not one provided by your distribution. MC64 has
not been licensed under a free license, it is only free for academic use. Thus the
distributions remove the according header.  
All GMRES examples are excluded from building using the `build_tests` and automatic
testing using CTest. You have to compile and run these examples manually.

All examples are a free flow coupled with a Darcy flow. The free flow consists of a
single-phase non-isothermal compositial (steam) air flow. The Darcy flow consists
of a two-phase (air, water) two-component (air, steam) non-isothermal flow.

* __test_evaporationpipe__:
  Default setup for soil-water evaporation, similar to experiments described in Mosthaf 2014 et al.
  Free flow in a two-dimensial pipe (8m*0.25m) above a sand-filled and fully water-saturated
  box (0.25m*0.25m). Introduced in section 5.1.1 of the thesis, further simulations
  are compared to this one.
* __test_evaporationpipere2500__, __test_evaporationpipere500__, __test_evaporationpipere50__, __test_evaporationpipere5__, __test_evaporationpipere05__:
  Variations of Reynolds numbers with simplified setup, section 5.1.2.
* __test_evaporationpipe3d__:
  Various 2.5- and three-dimensional pipe and channel setups used in section 5.1.3.
* __test_evaporationpipesuperlu__, __test_evaporationpipe3dsuperlu__, __test_evaporationpipegmres__, __test_evaporationpipe3dgmres__:
  Different solvers for the linear system. When using the GMRES examles, note the comment above
  concerning vanilla SuperLU and the need to adjusted the include. From section 5.2.
* __test_ventilationgallery__, __test_ventilationgalleryconstlambda__:
  Ventilation gallery of a subsurface radio-active desposal site. The gallery is 100m long and 5m high,
  covered with 1m concrete and 9m rock. Section 5.3.
* __test_channelfuelcell__:
  Two gas channels from a fuel cell with a porous bed connecting them, 3mm x 2 mm x 1mm, section 5.4.1.
* __test_fuelcellgeometry__:
  More complex geometry of two different gas channel layouts, 11mm x 11m x 1mm, section 5.4.2.

In order to run an executable use the script `executeGrueninger2017a.sh`:
```bash
./executeGrueninger2017a.sh test_evaporationpipe
```

Used Dune module versions
=========================

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/multidomain/promogruenich/test_channelfuelcell.cc,
  appl/multidomain/promogruenich/test_evaporationpipe.cc,
  appl/multidomain/promogruenich/test_fuelcellgeometry.cc,
  appl/multidomain/promogruenich/test_ventilationgallery.cc,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/multidomain/promogruenich/test_channelfuelcell.cc,
  appl/multidomain/promogruenich/test_evaporationpipe.cc,
  appl/multidomain/promogruenich/test_fuelcellgeometry.cc,
  appl/multidomain/promogruenich/test_ventilationgallery.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

dune-common               tags/v2.4.2     23e568987d8f24d2b188cd664a9fe51ccdc1393f
dune-geometry             tags/v2.4.2     4330ebc930121949ce5e5b3918e26f5d4c8922a1
dune-grid                 tags/v2.4.2     6d74754d667f093979f19687b1c76450461925bd
dune-localfunctions       tags/v2.4.2     cf91cc815324f24a3abe3356a55aa7a92b6f65e3
dune-istl                 tags/v2.4.2     c29b037dc2ae794d0810c532aa8ff6a493032970
dune-typetree             releases/2.3    ecffa10c59fa61a0071e7c788899464b0268719f
dune-pdelab               releases/2.0    19c782eea7232e94849617b20dfee8d9781eb4fb
dune-multidomaingrid      releases/2.3    3b829b7a130473749b8af2d402eaef1eff1071a7
dune-multidomain          releases/2.0    e3d52982dc9acca9bf13cd8f77bf0329c61b6327
dumux                     master          efb753d4cc3c0e96aad8430ffb66ea0519b372c7
