\setchapterpreamble[o]{%
\dictum[Marie Skłodowska Curie, 1894]{
Człowiek nigdy nie ogląda się na to, co zrobione,
ale na to patrzy, co ma przed sobą do zrobienia.}}

\chapter{Finale}
\lettrine{W}{e} introduced the relevant physical processes in the compositional
non-iso\-ther\-mal free flow and the two-phase compositional non-isothermal porous-medium flow.
We described how we model these using partial differential equations, closing conditions and
material laws. We emphasized how both the Navier-Stokes equation and the Darcy equation play an important
role. Then we presented the coupling conditions based on a local thermodynamic equilibrium and
the Beavers-Joseph-Saffman condition, following the approach from \citet{mosthaf2011}.

We use a cell-centered finite volume method on an axially parallel grid to discretize the
partial differential equations. For the Navier-Stokes equation, we use the marker and cell
scheme which moves the degrees of freedom for the velocities towards the edges of the grid
elements, forming one secondary, staggered grid per dimension. The coupling conditions are
applied without additional variables along the coupling interface. They are incorporated as
Dirichlet, Neumann or Robin boundary conditions, all of which result in interfaces fluxes.

For the porous-medium flow we use the finite volume implementation provided by \Dumux.
The marker and cell scheme is implemented using PDELab. The grid is split into two subdomains,
where the elements on both sides of the coupling interface match, simplifying the implementation.
The coupling is provided by a \Dune-Multidomain local coupling operator. The time integration
is approximated with an implicit Euler scheme. All contributions are compiled in one non-linear
system. This is linearized by a Newton method.

The resulting nonsymmetric sparse matrices are solved with direct methods. We investigated
iterative methods and tested promising ones: An algebraic multigrid (AMG) method, a Schur
complement method, and a GMRES preconditioned with MC64 and an incomplete LU factorization
with threshold and pivoting (ILUTP). We experienced problems with AMG's error criteria
leading to convergence problems. The Schur complement method is slow as we lack a
preconditioner for the not explicitly calculated Schur complement. GMRES with ILUTP shows
a restriction on the time step size for larger problems; the reason remains unclear.

The numerical results presented in the previous chapter show that there is a wide range of
applications that our implementation is capable of simulating. Three applications are then
simulated: A laboratory experiment to investigate
soil-water evaporation, the water content of the concrete and rock covering a ventilation gallery
of a deep geological repository for nuclear waste, and the simplified water management of a fuel
cell. These simulations cover time scales which span from
seconds to decades and length scales which range between nanometers and dozens of meters. We are
able to simulate setups in two and three dimensions, including complex geometries as long as they
can be approximated with an axially parallel grid, using various porous-medium types, and with
free flows spanning several magnitudes of Reynolds numbers.

Compared to the work of our predecessors \citep{mosthaf2011,baber2012}, we obtain a speedup for the
simulations between one and two orders of magnitude with comparable results \citep{grueninger2017}
and without oscillations in the free flow.

Regarding the applications, we have determined that the Reynolds number has an influence on the evaporation
rate. If the free flow channel or pipe is wider than the porous medium, a simulation using a
two-dimensional simplification may be flawed, depending on the Reynolds number. The evaporation rate
in the ventilation gallery differs by a factor of up to ten and the cumulative evaporation rate
by 50\% when compared with the results from \citet{masson2016}.
That being said, we can confirm the overall result of the given setup, where the water saturation
in the rock is not altered, and only the concrete ceiling partially dries.
For the fuel cell with an interdigitated
flow field, we could show that parts of the porous medium accumulate water which might reduce the
efficiency of the fuel cell. Further investigations and insights will be reachable after
the electrochemistry is included.

\section{Conclusions}
We draw the following conclusions regarding the coupled Navier-Stokes and Darcy flow in general,
particularly our discretization and implementation

\subsubsection{Material laws}
Our implementation heavily relies on the material laws provided by \Dumux. We adopted the set of
material laws used by the predecessor code. We altered some material laws---for example, the law used to compute
the effective thermal conductivity $\lambda_\text{pm}$, or the assumption of incompressible flow in the
free-flow subdomain---to either reduce computational costs or to diminish the number of nonzero matrix entries. We
tried to keep our results close to the results from the predecessor code, and did not systematically
examine all used material laws. As the example in section \ref{sec:ventilationGallery} shows,
changing some laws will marginally impact the result. Further simplifications could have two advances: First, one could
reduce the computational costs of the matrix assembly. Second, it would lower the bar for other
scientific groups to reproduce our results as implementing the used material laws is a major obstacle.
Still, one has to balance every simplification against the applicability to the different applications.
If the implementation becomes more focused on a specific example, it can exploit the special
circumstances at hand.

\subsubsection{Free flow}
The used first-order upwind scheme introduces severe numerical diffusion which prevents eddy detachment
as the fine grid required leads to linear systems too large to solve. Replacing the upwind scheme
with a total variation diminishing (TVD) or a higher-order scheme would improve this, but we cannot include
such schemes due to PDELab's ability to only access face-neighboring degrees of freedom.
In general, when detached eddies occur near the interface, the validity of the coupling conditions must
be reviewed.

In this work, the grid resolution and the consequential large number of degrees of freedom limit the free flow to laminar
flows with moderate Reynolds numbers. To investigate flows with higher Reynolds numbers, the implicit
scheme can be replaced by a semi-implicit predictor-corrector scheme \citep{versteegMalalasekera2007} and a higher-order scheme.
This conflicts with the implicit coupling of free flow and porous-medium flow. More suitable methods
like spectral methods or discontinuous Galerkin methods require appropriate coupling conditions. The
general approach by \citet{kanschatRiviere2010} might be of avail.

The examined applications all have a stationary flow field, no change in the boundary conditions over time,
and no processes within the domain that affect the flow field. Precomputing the velocity once,
similar to \citet{masson2016}, would reduce
the computational cost for both the assembly and the linear methods, especially as the linear system
would no longer contain a saddle-point problem. Note that applications with transient flow fields exist
like soil-water evaporation using measured wind velocities, or biological applications considering
breathing or heartbeats.

\subsubsection{Solving the system of linear equations}
The limiting factor remains the solver for the system of linear equations. The direct
solver UMFPack is currently the preferred choice. The easiest way to raise the bar with simulating larger problems
is to use better computers. Instead of a desktop computer which we used for the simulations in this work, a workstation or a
cluster node with the latest hardware and more memory can cope with larger linear systems. The
thread-parallel SuperLU\_MT might decrease the time for solving the system, but only by the number of
possible threads. Nevertheless, the memory limit, common for all direct methods, remains. Further progress
with direct solvers \citep{agullo2013}, especially utilizing the enormous computational capacities of
graphics processors \citep{davis2016}, will reduce the time to solve the occurring linear systems in the future.

The use of iterative methods to speed up the solution of linear systems still needs more work.
The investigated
combination of MC64 reordering and ILUTP can probably be improved with a more suitable choice of
parameters, especially regarding the
restriction on the time step size. A less frequent calculation of the preconditioner carries potential;
the MC64 reordering is calculated every time step and the ILUTP preconditioner every Newton step, which
can be done less often. As this reduces the quality of the preconditioner, one must, in turn, take
care to ensure the robustness of the overall method.

The algebraic multigrid methods are worthy of a more in-depth investigation. This also applies to the
Schur complement methods, for which we lack a cheap approximation of the Schur complement. Both approaches
promise a lower computational complexity compared to the LU decomposition of the direct solvers and the
ILU preconditioners. Additionally, the used algebraic multigrid method scales well for parallel
computations \citep{blatt2010}.

\subsubsection{Decoupling time steps}
The idea to decouple the two domains, and to compute the free flow more often was proposed
by \citet{rybak2014} and \citet{rybak2015}. This idea stems from the fact that the porous-medium
flow is much slower, and requires updates less frequently. If it were solved less often, it would
reduce the overall computational cost. For the examined applications, the benefit is negligible.
Regarding the two-dimensional setup from section \ref{sec:twoDimensional}, the porous medium is accountable
for only 10\% of the degrees of freedom and its matrix assembly causes less than a sixth of the overall time spend for
the assembly. For the other evaporation pipe setups, the free flow takes up even a larger share of both degrees
of freedom and assembly time. This is true as, in the free flow, the time steps are keep small, the values change little,
and no additional Newton steps are required by the nonlinear material laws. For the fuel cell, the appearance
and disappearance of the liquid phase triggers the computation of additional Newton solver steps. The time
step size is limited by the physics of the porous medium and not by the free flow. As the air crosses the
porous medium, both physical compartments have similar time and length scales.

This does not mean that
auspicious examples do not exists. Simulating the impact to the surface of a CO$_2$ storage with a leakage
\citep{oldenburg2004} requires a large porous-medium subdomain, compared to the one for the free flow. For such setups
with large Darcy domains, the influence of the free flow can maybe be incorporated as top boundary
conditions \citep{tang2013}.

\section{Outlook}
This work can be the basis for further research which might lead to a continued development of the
software. The software can be extended---similar to the extensions to the predecessor code---to include
salt precipitation for soil water evaporation \citep{jambhekar2015}, drop formation at the interface for
fuel cells \citep{baber2014}, surface roughness \citep{fetzer2012}, boundary layer models or
Reynolds-averaged Navier-Stokes methods with eddy viscosity models like zero-equation models
\citep{fetzer2016} or a $k$-$\varepsilon$ model. Further possible extensions to the model could include solar
radiation, which should be considered for soil-water evaporation in arid regions, as well as a way to include
rainfall for the recharge of the soil water, which could include a tertiary coupled shallow
water model \citep{sochala2009} that might require a coupling including vertical momentum
transfer \citep{furman2008}. One could conduct a simulation of soil-water evaporation for a
field with measured weather data, especially temperature, air humidity and wind velocity. This
could also include day-night cycles and condensation of vapor caused by nocturnal cooling.
Another possible extension is evapotranspiration, which also considers transpiration, i.\,e.,
living plants cause vaporization of water through their stomata. This affects the evaporation
rate of surfaces covered with plants. A far-distance goal could be a complete simulation of the
grain yield of a field, covering its water dynamics, root-water uptake, and crop growth
\citep{zhang2015}. Future research could also investigate the effect of vegetation on the
turbulence, e.\,g., it is reduced by the porous bed while it is increased by grass
\citep{pechlivanidis2015}. Biological applications would be interesting as well.

The software can be improved by parallelization of the assembly and the solving. Replacing
PDELab and \Dune-Multidomain would lift the restriction on face-neighboring degrees of freedom
which would allow for a replacement of the upwind scheme and would allow a decoupling of the
subdomains with a Robin-Robin coupling, as the Dirichlet-Neumann coupling shows poor convergence
\citep{ackermann2016,discacciati2004a}. For the marker and cell scheme, a generalization exists
for triangles and tetrahedra or prisms respectively \citep{nicolaides1989,nicolaides1993};
while there is a restriction on the angles to obtain valid dual grids, its use would allow
for geometries more general than axially parallel geometries.
\\
To use a state-of-the-art discretization for the free flow together with \Dumux, one has
to consider coupling non-\Dune software. \citet{bungartz2016} describe preCICE which is a
general framework for coupling different grid-based numerical simulators. It would be handy
to couple \Dumux with established free-flow simulators like OpenFOAM, Comsol or Fluent. Even without utilizing
preCICE, the described ideas of data interchange, non-matching grids, and coupling schemes
are useful by themselves.

To attract people to develop methods for our system of linear equations, we submitted matrices from a
two- and a three-dimensional coupled evaporation pipe test case to the SuiteSparse Matrix Collection,
formerly known as the University of Florida Sparse Matrix Collection \citep{davis2011}. Matrices from
this collection where used for benchmarks of linear solvers in \citet{benzi2000} and \citet{li2011}.

There is a lack of test cases with generally recognized results. The scientific groups have
their own test cases, but they are seldom verified by other groups. If differences occur, like
the ones described in section \ref{sec:ventilationGallery}, it is unclear which result is preferred.
It would be great to announce a benchmark problem that can be simulated by many groups and
which can be set up in a laboratory to obtain data for comparison. Analytic solutions are
probably out of reach, but direct numerical solutions can act as referee.

\subsubsection{Invitation to use our results and our software}
Hopefully we have made a fruitful contribution to the field of coupled Navier-Stokes flow with Darcy flow.
We want other researchers to use our results to improve their work. We invite everybody to utilize
our implementation, either for comparison with new numerical codes or to simulate applications
of their own.
