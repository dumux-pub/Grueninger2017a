\setchapterpreamble[o]{%
\dictum[Johann Wolfgang von Goethe, 1809]{
\foreignlanguage{ngerman}{
Das Wasser ist ein freundliches Element für den,
der damit bekannt ist und es zu behandeln weiß.}}}

\chapter{Motivation}
\lettrine{W}{ater} is essential to life as mankind knows it \citep{ball2005}. We
are surrounded by water; it is part of all humans, animals and plants, as well as
in the atmosphere and in the ground below our feet. Only 35~percent to 40~percent of the
world's precipitation runs off to rivers and lakes or seeps into the soil,
meaning that the remaining balance evaporates to the atmosphere. On average, one
meter of water evaporates every year. This has an
impact on Earth's energy budget; more than half of the net solar radiation over the
land surface is disposed towards evaporation. In comparison, photosynthesis usually
dissipates less than one percent of the net solar radiation, with five percent
dissipation at the maximum. \citep{brutsaert1982} Astonishingly, the knowledge
about this ubiquitous process is inadequate. \citet{rind1997}
report on the discrepancy between measurements and several evaporation modeling
approaches. In order to determine future water availability, accurate estimates of
the moisture transport are crucial.

The evaporation of a fluid inside a porous medium to the surrounding atmosphere is a more
general problem. We are going to investigate the flow of dry air parallel to a sand-filled
box. This porous medium has a sharp margin and can contain both gas and liquid.
The liquid inside the porous medium evaporates and diffuses into the free flow.
Outside the porous medium the liquid only occurs vaporized in the gas as a component.
Figure~\ref{fig:generalProblem} provides a schematic diagram of such a problem. The
setup and our numerical examples are based on a lab experiment performed by
\citet{mosthafHelmigOr2014}. For a photograph of the setup see figure~\ref{fig:zurichWindchannel}.
This general setup has a wide range of applications and is not limited to
investigations of the evaporation of soil water.

\begin{figure}
\centering
\subfloat[]{
\label{fig:generalProblem}
\begin{tikzpicture}[scale=1.46]
  \fill[pattern=north west lines] (-0.1,-2.1) rectangle ++(4.2,2.1);
  \fill[white] (0,-2) rectangle ++(4,2);
  \begin{scope}
    \clip(0,2) rectangle ++(4,-4);
    % porous medium circles
    \def\radius{0.1}
    \pgfmathsetmacro{\offsety}{sqrt(3)*\radius}
    \pgfmathsetmacro{\stepwidthx}{2*\radius}
    \pgfmathsetmacro{\stepwidthy}{2*\offsety}
    \foreach \x in {0,\stepwidthx,...,4.1}
      \foreach \y in {0,-\stepwidthy,...,-2}
      {
        \draw[gray,fill=gray](\x+\radius,\y-\radius) circle (\radius*0.8);
        \draw[gray,fill=gray](\x,\y-\radius-\offsety) circle (\radius*0.8);
      }
    % boundary layer
    \fill [black!10,
          domain=0.2:4,
          smooth,
          samples=40
    ] plot ({\x},{0.1*sqrt(\x-0.2)}) -- (4,0) -- cycle;
    % velocity field
    \draw [gray,
          domain=0:2,
          smooth,
          samples=40
    ] plot ({0.5*\x},{\x^4});
    \draw [gray,->] (0,0.25) -- ++({0.5*sqrt(sqrt(0.25)) - 0.02},0);
    \draw [gray,->] (0,0.75) -- ++({0.5*sqrt(sqrt(0.75)) - 0.02},0);
    \draw [gray,->] (0,1.25) -- ++({0.5*sqrt(sqrt(1.25)) - 0.02},0);
    \draw [gray,->] (0,1.75) -- ++({0.5*sqrt(sqrt(1.75)) - 0.02},0);
    % evaporation
    \draw [gray,decorate,decoration=snake,->] (1.2,0) -- ++(0,0.7);
    \draw [gray,decorate,decoration=snake,->] (1.6,0) -- ++(0,0.7);
    \draw [gray,decorate,decoration=snake,->] (2.0,0) -- ++(0,0.7);
  \end{scope}
  \draw (0,0) -- ++(4,0);
  \draw (0,2) rectangle ++(4,-4);
  \draw (4,2) node [below left]{free flow};
  \draw (4,-2) node [above left,fill=white,fill opacity=0.8,text opacity=1]{porous medium flow};
\end{tikzpicture}
}
\quad
\subfloat[]{
\label{fig:zurichWindchannel}
\includegraphics[width=9.2cm]{images/klausZurich.jpg}
\begin{tikzpicture}
  \node[rotate=90,inner sep=0] at (0,0) {\tiny{from \citet{mosthafHelmigOr2014}. Modifications: crop, colors}};
\end{tikzpicture}
}
\caption[General problem of coupled Navier-Stokes/Darcy flow]
        {General problem of coupled Navier-Stokes/Darcy flow
         (a)~schematic diagram with evaporation from the porous medium
         and evolved boundary layer
         (b)~laboratory experiment motivating our
         simulation, taken at ETH Zurich, group of Danny Or}
\end{figure}

There are several applications related to soil science: In arid regions with
high evaporation potential, irrigated fields are
susceptible to salinization which lowers or destroys their soil
fertility. \citet{jambhekar2015} simulate the evaporation of soil water to analyze
this evaporative salinization. Another application is the underground storage of
supercritical carbon dioxide, as part of carbon dioxide capture and storage
(CCS). \citet{oldenburg2004}
investigated the consequences of a leakage in the cap rock, leading to the release
of carbon dioxide to the atmosphere where it forms a plume, see figure~\ref{fig:ccs}.
The authors found out that the CO$_2$ concentration would remain low enough to
not endanger human life on the
surface. Often, the solution outside the porous medium is of no importance, just
its effect inside the porous medium. So-called top boundary conditions approximately
account for the effect of the evaporation without its computational costs. These
boundary conditions are developed by examining the coupled problem \citep{tang2013}.
Besides storing carbon dioxide in the subsurface, engineers plan to build underground
end disposal facilities for their nuclear waste, too. \citet{zhang2015b} and \citet{masson2016}
try to predict the evaporation of water from the encircling rock to excavated
ventilation galleries. This helps to estimate the structural integrity of the rock
and the galleries for at least a hundred years. Humans have buried another legacy:
100 million landmines endanger the life of civilians, especially children.
Landmines alter soil moisture and temperature which makes them detectable at
the surface \citep{smits2013}.

The setup can also be applied to technical problems. The industrial drying of wood, brick,
food and similar materials is energy consuming. For several countries, the related
energy consumption accounts for 10\% to 25\% of total industrial energy consumption.
\citet{defraeyeDryingReview2014} gives an overview on published research in this field.
Better simulations could reduce the energy consumption and improve the quality of the
dried goods. Closely related is refrigeration
of food bulk \citep{verboven2006}. In some cases, the amount of energy required for
evaporation is not a negative quality, it can be utilized as transpiration cooling of rocket engines
to protect engine parts from too high temperatures for increased reliability and fuel
economy \citep{dahmen2014}. In fuel cells, based on proton exchange membranes (PEM), the
membranes are surrounded by a thin porous medium where as little as possible liquid
water should remain, see figure~\ref{fig:fuelCellMotivation}. The liquid water blocks
the gases for the proton exchange and must be transported away in the gas channel
\citep{baber2012}. \citet{cimolin2013}
model a ventilation channel inside a motorcycle helmet, see figure~\ref{fig:helmet}.
The channel transports fresh air to the comfort tissue for cooling and evacuation of
sweat. It prevents overheating of the driver's head and fogging of the visor. Another
group of applications are biological problems, for example, the simulation of a pre-lens
tear film on a contact lens \citep{usha2013} or blood vessels surrounding organs and
tissue \citep{discacciati2009}.

\begin{figure}
\centering
\subfloat[]{
\label{fig:ccs}
\begin{tikzpicture}[scale=1.3]
  % soil layers
  \fill[black!40] (0,0) rectangle ++(5,1);
  \fill[gray] (0,1) rectangle ++(5,0.5);
  \fill[black!40] (0,1.5) rectangle ++(5,0.25);
  \fill[gray] (0,1.75) rectangle ++(5,0.75);
  % injection
  \fill [white](0.5,2.6) rectangle ++(0.05,-2.2);
  \draw (0.5,2.6) -- ++(0,-2.2);
  \draw (0.55,2.6) -- ++(0,-2.2);
  % main plume
  \fill [darkgray,
         domain=0:4,
         smooth,
         samples=60
        ] plot ({0.55+\x},{0.4+0.3*sqrt(\x)})
    -- (0.55,1);
  \fill [darkgray,
         domain=0.5:0,
         smooth,
         samples=15
        ] plot ({0.5-\x},{0.4+0.3*sqrt(\x)})
    -- (0.5,1) -- (0,1);
  % small plumes and fault
  \fill [darkgray,
         domain=0:1,
         smooth,
         samples=60
        ] plot ({2.7+\x},{1.6+0.3*sqrt(\x/4)})
    -- (2.55,1.75);
  \fill [darkgray,
         domain=1:0,
         smooth,
         samples=60
        ] plot ({2.7-\x},{1.6+0.3*sqrt(\x/4)})
    -- (2.55,1.75);
  \draw (2.5,1) -- ++(0.5,1.5);
  \fill [darkgray,
         domain=-0.1:0.1,
         smooth,
         samples=60
        ] plot ({2.97+\x},{2.5-0.1+10*\x*\x});
  % plume
  \fill [black!10,
         domain=1:3.13,
         smooth,
         samples=60
        ] plot ({1.87+\x},{2.5+0.6*log10(\x)})
    -- (5,2.5);
  % labels
  \draw [<-] (0.525,2.8) -- ++(0,0.3) node[right,xshift=0.2cm,above]{injection};
  \draw [->] (2.7,1.1) -- ++(0.11,0.33) node[below right]{leakage};
  \draw (4,2.9) node{plume};
  \draw (5,4) node[below left]{atmosphere};
  \draw (5,0) node[above left]{soil};
  \draw (2,2.5) -- ++(0,1.5);
  \draw (2,2.5) -- ++(0.22,0.05) -- ++(0,1.45);
  \draw [->](2,2.65) -- ++(0.2,0);
  \draw [->](2,3.05) -- ++(0.2,0);
  \draw [->](2,3.45) -- ++(0.2,0);
  \draw [->](2,3.85) -- ++(0.2,0);
  % frame
  \draw (0,0) rectangle ++(5,4);
\end{tikzpicture}
}
\qquad \quad
\subfloat[]{
\label{fig:helmet}
\begin{tikzpicture}[y=0.8, x=0.8, yscale=-0.4, xscale=0.4]
  % ventilation channel
  \path[fill=black!10] (249.7088,179.1105) .. controls (238.3818,171.2042) and
    (229.1086,164.6551) .. (229.1017,164.5569) .. controls (229.0786,164.2249) and
    (237.3403,154.7588) .. (240.5532,151.4360) -- (243.7435,148.1365) --
    (254.4401,156.1682) .. controls (260.3233,160.5855) and (265.2422,164.1998) ..
    (265.3710,164.1998) .. controls (265.4999,164.1998) and (267.3911,162.4721) ..
    (269.5738,160.3605) .. controls (298.3845,132.4885) and (332.4583,114.2493) ..
    (369.4347,106.9065) .. controls (398.4758,101.1395) and (428.6429,102.9228) ..
    (456.6244,112.0607) .. controls (469.0637,116.1229) and (484.4921,123.1714) ..
    (495.5416,129.8400) .. controls (498.2129,131.4521) and (500.5799,132.7712) ..
    (500.8016,132.7712) .. controls (501.0232,132.7712) and (506.6877,130.6540) ..
    (513.3893,128.0662) .. controls (524.6942,123.7009) and (525.6339,123.3979) ..
    (526.4047,123.8698) .. controls (529.6912,125.8818) and (547.6490,142.6465) ..
    (547.6490,143.7026) .. controls (547.6490,144.0434) and (506.8120,162.1760) ..
    (506.0445,162.1760) .. controls (505.8436,162.1760) and (504.0665,161.2018) ..
    (502.0953,160.0111) .. controls (458.9711,133.9618) and (410.3302,126.6312) ..
    (363.0061,139.0492) .. controls (331.2000,147.3953) and (299.7307,165.2280) ..
    (275.7206,188.5113) .. controls (272.8994,191.2471) and (270.5264,193.4855) ..
    (270.4473,193.4855) .. controls (270.3682,193.4855) and (261.0359,187.0167) ..
    (249.7088,179.1105) -- cycle;
  % comfort tissue
  \path[fill=gray,line join=miter,line cap=butt]
    (294.0957,172.6949) .. controls (318.9436,153.9225) and
    (348.3561,141.2698) .. (379.0443,135.9732) .. controls (400.5925,132.2542) and
    (422.8177,132.1338) .. (444.2857,136.2908) .. controls (467.4594,140.7780) and
    (489.7180,150.3029) .. (508.5714,164.5051) .. controls (532.2160,182.3163) and
    (550.0711,207.1187) .. (562.1429,234.1479) .. controls (578.6867,271.1903) and
    (584.6945,312.8697) .. (579.2857,353.0765) -- (549.1071,353.0765) .. controls
    (558.1574,303.5590) and (544.9493,250.4007) .. (513.7754,210.8776) .. controls
    (498.6816,191.7413) and (479.2923,175.5728) .. (456.5933,166.6974) .. controls
    (437.5053,159.2339) and (416.5231,157.1135) .. (396.1468,159.3186) .. controls
    (375.7705,161.5236) and (355.9793,167.9640) .. (337.5864,177.0060) .. controls
    (323.5031,183.9293) and (310.1402,192.4122) .. (298.3158,202.7299) .. controls
    (289.9526,210.0274) and (282.3774,218.2275) .. (275.7644,227.1418) --
    (254.5618,212.8122) .. controls (265.6978,197.6143) and (279.0626,184.0523) ..
    (294.0957,172.6949) -- cycle;
  % visor
  \path[draw=gray,line join=miter,line cap=butt,even odd rule]
    (188.9732,236.6926) -- (308.5714,235.2193) -- (398.5714,223.7908) --
    (407.1429,279.5051) -- (305.7143,365.2193) -- (164.2857,420.9336);
  % helmet contour
  \path[draw=black,line join=miter,line cap=butt,even odd rule]
    (204.0625,533.3443) .. controls (277.1915,493.4974) and (353.8548,460.1396) ..
    (432.8571,433.7908) .. controls (489.4333,414.9215) and (547.2067,399.6426) ..
    (605.7143,388.0765);
  \path[draw=black,line join=miter,line cap=butt,even odd rule]
    (204.2857,533.7908) .. controls (188.7982,508.6061) and (177.1914,481.0400) ..
    (170.0000,452.3622) .. controls (151.9098,380.2220) and (162.3968,302.8526) ..
    (190.0000,233.7908) .. controls (202.0468,203.6503) and (217.5634,174.3894) ..
    (240.0000,150.9336) .. controls (270.1915,119.3707) and (311.5591,99.9993) ..
    (354.2857,90.9336) .. controls (383.6088,84.7119) and (414.0604,83.0653) ..
    (443.6470,87.8803) .. controls (473.2337,92.6954) and (501.9384,104.1068) ..
    (525.7143,122.3622) .. controls (557.3557,146.6569) and (579.0982,182.0604) ..
    (592.8571,219.5051) .. controls (612.5638,273.1363) and (616.9203,332.3115) ..
    (605.2801,388.2504);
  % arrows
  \draw[->] (242,160) -- ++(20,16);
  \draw[->] (330,135) -- ++(20,-8);
  \draw[->] (410,120) -- ++(24,2);
  \draw[->] (505,148) -- ++(22,-10);
  % legend
  \fill[black!10] (480,450) rectangle ++(27,27);
  \draw (507,460) node[right]{ventilation channel};
  \fill[gray] (480,500) rectangle ++(27,27);
  \draw (507,510) node[align=center,right]{comfort tissue};
\end{tikzpicture}
}
\caption[Further applications for the setup]
        {Further applications for the setup
         (a)~carbon dioxide stored in a geological formation, leakage leads to
         a CO$_2$ plume in the atmosphere
         (b)~motorcycle helmet with an included channel for cooling next to
         the comfort tissue, after \citet{cimolin2013}}
\end{figure}

\section{Classification}
As various applications exist, there are plenty of publications describing the
simulation of the coupled Navier-Stokes and Darcy flow to explore these
evaporation processes. We are going to present the objectives of this
work, related works using the Beavers-Joseph condition for the coupling, and
alternative coupling approaches.

\subsection{Objectives}
We want to provide a numerical simulator to investigate evaporation of soil
water under the influence of wind streaming along the surface. The simulator
incorporates all relevant processes in the soil and atmosphere. The atmospheric
processes are a Navier-Stokes flow of air with vapor transport. The processes
included within the soil are a two-fluid-phase Darcy flow of water and air, the
transport of one substance as a component of the other, and the evaporation
of water. Both parts involve an energy balance so as to track the temperature,
see figure~\ref{fig:phases}.

The parts are coupled with a sharp interface using a Beavers-Joseph condition.
We want to discretize the system of partial differential equations with
robust numerical, grid-based schemes, and have chosen the finite volume
method (FVM) and the marker and cell (MAC) scheme in order to do so. This
enables simulations of realistic setups.
The whole discretized system is linearized with Newton's method, resulting in
a monolithic coupling without the need to iterate between atmospheric and
soil parts to solve the system. The system of linear equations is either
solved by a direct method, or uses an iterative method. The later
performs a reordering of the matrix to make it diagonally dominant,
preconditions with an incomplete LU factorization, and solves the system
with an iterative Krylov subspace method. This is proposed by \citet{duff2001}
and is successfully tested for various problems in \citet{benzi2000}.

With this, we demonstrate the capability to efficiently simulate lab
experiments, a fuel cell, and a part of a geological repository for
nuclear waste. These simulations include complex geometries and
three-dimensional problems.

\begin{figure}
\centering
\begin{tikzpicture}
  % vertical alignment
  \tikzset{style={font=\vphantom{Ag}}}
  %% free flow
  % gaseous phase
  \draw (1.9,2.7) rectangle ++(2.5,-1.6);
  \draw (1.9,2.1) -- ++(2.5,0);
  \draw (1.9,2.4) node[right]{gaseous};
  \draw (1.9,1.8) node[right]{air};
  \draw (1.9,1.4) node[right]{vapor};
  % labels
  \draw (4.75,2.4) node[right]{temperature};
  \draw (4.65,2.4) node{+};
  %% Darcy flow
  % gaseous phase
  \draw (0.5,-0.5) rectangle ++(2.5,-1.6);
  \draw (0.5,-1.1) -- ++(2.5,0);
  \draw (0.5,-0.8) node[right]{gaseous};
  \draw (0.5,-1.4) node[right]{air};
  \draw (0.5,-1.8) node[right]{vapor};
  % liquid phase
  \draw (3.5,-0.5) rectangle ++(2.5,-1.6);
  \draw (3.5,-1.1) -- ++(2.5,0);
  \draw (3.5,-0.8) node[right]{liquid};
  \draw (3.5,-1.4) node[right]{water};
  \draw (3.5,-1.8) node[right]{air};
  % labels
  \draw (6.35,-0.8) node[right]{temperature};
  \draw (3.25,-0.8) node{+};
  \draw (6.25,-0.8) node{+};
  \draw[->,thick] (3.1,-1.6) -- ++(0.3,0);
  \draw[<-,thick] (3.1,-1.75) -- ++(0.3,0);
  %% coupling
  \draw [thick](-3,0.3) -- ++(12,0);
  \draw (-3,0.3) node [above right]{Navier-Stokes flow};
  \draw (-3,0.3) node [below right]{Darcy (porous medium) flow};
  \draw[ultra thick,white] (3.15,-0.25) -- ++(0,1.1);
  \draw[ultra thick,white] (3.3,-0.25) -- ++(0,1.1);
  \draw[->,thick] (3.15,-0.25) -- ++(0,1.1);
  \draw[<-,thick] (3.3,-0.25) -- ++(0,1.1);
  \draw (3.3,0.3) node[above right]{interface coupling conditions};
\end{tikzpicture}
\label{fig:phases}
\caption[Phases and their components as they occur in the two subdomains]
        {Phases and their components as they occur in the two subdomains.
         Each box stands for a phase with its components.}
\end{figure}

\subsection{Related works}
Several groups have performed experiments related to this coupled problem. Most
experiments are confined to single-phase and isothermal setups, for example, the
flow over a porous bed made up of regular cylinders \citep{prinos2003}, velocity
measurements close to the interface of a stack of spheres \citep{okrajac2009},
or particle image velocimetry (PIV) measurements to understand better turbulence
over porous media \citep{suga2016}.
\citet{dahmen2014} measure the temperature with an infrared camera.
\citet{davarzani2014} and \citet{mosthafHelmigOr2014} perform experiments using
a wind channel over a porous, water-filled bed to measure data for simulations
of evaporation. These experiments motivate the simulation setup of this work.
\citet{shahraeeni2012} identify several characteristics of the evaporation
rate of such systems and explains possible reasons.
\citet{defraeye2016} survey the water content of a drying apple slice with
neutron imaging.

From a mathematical point of view, the coupling of mass and momentum transfers
with the Beavers-Joseph condition is a challenging problem. \citet{discacciati2009}
give an overview of more than a dozen analyses and further related experimental
and applied works. Examples of such articles include an asymptotic analytic solution
for a coupled problem \citep{jager2001}, a proof of existence and convergence
of a weak scheme \citep{layton2002}, and a coupling scheme for
various discontinuous Galerkin methods \citep{kanschatRiviere2010}. All of these
works are limited to single-phase isothermal problems. But there are exceptions.
In \citet{cesmelioglu2012}, the authors proof the
existence of a weak solution of a coupled Stokes/Darcy problem with a component
transport. And \citet{ervin2015} examine a quasi-stationary Stokes/Darcy system again
with a component transport. We are not aware of any paper presenting mathematical
proofs concerning coupling and energy transport or two-fluid-phase Darcy flow.

Several works address the efficiency of coupled Stokes/Darcy simulations.
\citet{chidyagwaiRiviere2011} propose the use of a two-grid method.
\citet{badea2010} compare different linearizations for the coupled problem and
report the convergence of Newton's method to be inferior to the ones of a fix-point
or a Richardson iteration. Iterative Dirichlet-Neumann coupling schemes between
the two subdomains are examined by \citet{discacciati2004a}, resulting in a harsh
limit concerning the time-step size. Numerical tests with our code base did not
show an advantage of the iterative single-phase coupling over a monolithic coupling
with respect to the computational effort, at least with a direct solver for the
linear system \citep{ackermann2016}. A Robin-Robin coupling yields a better
iterative coupling scheme \citep{discacciati2007,birgle2017}. As the Navier-Stokes
equation needs a finer temporal resolution, the coupled system can be calculated
with coarse time steps and simulate the atmospheric part in-between more often with
smaller time steps \citep{rybak2014,rybak2015}.
\citet{discacciati2004b} introduce a Schur complement preconditioner for
the coupled problem. More general preconditioners for coupled problems have been
proposed, for example, in \citet{howle2013}. There are publications using ILU
methods as preconditioner for saddle-point problems arising from the pure Navier-Stokes
equation without a coupling, but this solution strategy is known to have limitations
\citep{zeng1995,konshin2015}.

Without the burden of analyzing the complex system of partial differential
equations, applied scientists study coupled evaporation problems with numerical
software. The review article from \citet{defraeyeDryingReview2014} presents
20 different implementations of Stokes/Darcy coupling to simulate drying, but many
use simplifying assumptions compared to our setup. An early work is \citet{salinger1994}
simulating spontaneous ignition of coal stockpiles with a non-isothermal compositional
coupling. \citet{defraeye2012coupling} present
a non-isothermal two-phase compositional Darcy flow coupled to a non-isothermal
Stokes flow. The main difference here comes from the coupling, where the flow
in the porous medium is calculated first, the fluxes across the interface second,
and the free flow third. \citet{masson2016} have a similar setup, but pre-calculate the free flow
and only solve the energy and the component transport coupled to the
porous-medium flow. Compared to our setup, the work from \citet{dahmen2014}
does not include the vapor mass fraction transport in the free flow.
Our work is the successor of another implementation based on \Dumux
\citep{mosthaf2011,baber2012,mosthafHelmigOr2014,fetzer2016}. The main difference
is the used discretization scheme, namely a finite volume scheme compared to
their equal-order scheme which tends to oscillations in the Stokes flow.

\begin{figure}
\centering
\subfloat[]{
\label{fig:fuelCellSketch}
\begin{tikzpicture}[scale=0.63]
  \pgfmathsetmacro{\isoLX}{-0.8}
  \pgfmathsetmacro{\isoLY}{0.15}
  \pgfmathsetmacro{\isoRX}{0.35}
  \pgfmathsetmacro{\isoRY}{0.4}
  \pgfmathsetmacro{\isoZ}{0.4}
  % bottom gas diffusor
  \draw [fill=black!10] (5*\isoRX,5*\isoRY-3*\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (3*\isoRX,3*\isoRY-3*\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (1*\isoRX,1*\isoRY-3*\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (7*\isoRX,7*\isoRY-4*\isoZ) -- ++(-\isoRX,-\isoRY) -- ++(7*\isoLX,7*\isoLY) -- ++(\isoRX,\isoRY) -- cycle;
  \draw [fill=black!10] (5*\isoRX,5*\isoRY-4*\isoZ) -- ++(-\isoRX,-\isoRY) -- ++(7*\isoLX,7*\isoLY) -- ++(\isoRX,\isoRY) -- cycle;
  \draw [fill=black!10] (3*\isoRX,3*\isoRY-4*\isoZ) -- ++(-\isoRX,-\isoRY) -- ++(7*\isoLX,7*\isoLY) -- ++(\isoRX,\isoRY) -- cycle;
  \draw [fill=black!10] (1*\isoRX,1*\isoRY-4*\isoZ) -- ++(-\isoRX,-\isoRY) -- ++(7*\isoLX,7*\isoLY) -- ++(\isoRX,\isoRY) -- cycle;
  \draw [fill=black!10] (0,-4*\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (0,-5*\isoZ)
         -- ++(0,\isoZ) -- ++(\isoRX,\isoRY) -- ++(0,\isoZ) -- ++(\isoRX,\isoRY)
         -- ++(0,-\isoZ) -- ++(\isoRX,\isoRY) -- ++(0,\isoZ) -- ++(\isoRX,\isoRY)
         -- ++(0,-\isoZ) -- ++(\isoRX,\isoRY) -- ++(0,\isoZ) -- ++(\isoRX,\isoRY)
         -- ++(0,-\isoZ) -- ++(\isoRX,\isoRY) -- ++(0,-\isoZ) -- cycle;
  % arrow air outflow
  \draw [fill=black!85] (0.5*\isoRX+8.5*\isoLX,0.5*\isoRY+8.5*\isoLY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  \draw [fill=black!85] (2.5*\isoRX+8.5*\isoLX,2.5*\isoRY+8.5*\isoLY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  \draw [fill=black!85] (4.5*\isoRX+8.5*\isoLX,4.5*\isoRY+8.5*\isoLY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  \draw [fill=black!85] (6.5*\isoRX+8.5*\isoLX,6.5*\isoRY+8.5*\isoLY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  % cathode
  \draw [fill=black!50] (0,-2.65*\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,-0.35*\isoZ) -- ++(-7*\isoRX,-7*\isoRY) -- cycle;
  \draw [fill=black!50] (0,-2.65*\isoZ) -- ++(0,-0.35*\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,0.35*\isoZ) -- cycle;
  % membrane
  \draw [fill=black!70] (0,-2.35*\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,-0.3*\isoZ) -- ++(-7*\isoRX,-7*\isoRY) -- cycle;
  \draw [fill=black!70] (0,-2.35*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,0.3*\isoZ) -- cycle;
  % anode
  \draw [fill=black!30] (0,-2*\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(7*\isoLX,7*\isoLY) -- ++(-7*\isoRX,-7*\isoRY) -- cycle;
  \draw [fill=black!30] (0,-2*\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,-0.35*\isoZ) -- ++(-7*\isoRX,-7*\isoRY) -- cycle;
  \draw [fill=black!30] (0,-2*\isoZ) -- ++(0,-0.35*\isoZ) -- ++(7*\isoLX,7*\isoLY) -- ++(0,0.35*\isoZ) -- cycle;
  % arrows hydrogen outflow
  \draw [fill=white] (9.1*\isoRX+0.5*\isoLX,9.1*\isoRY+0.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  \draw [fill=white] (9.1*\isoRX+2.5*\isoLX,9.1*\isoRY+2.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  \draw [fill=white] (9.1*\isoRX+4.5*\isoLX,9.1*\isoRY+4.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  \draw [fill=white] (9.1*\isoRX+6.5*\isoLX,9.1*\isoRY+6.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  % top gas diffusor
  \draw [fill=black!10] (5*\isoLX,5*\isoLY-\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (3*\isoLX,3*\isoLY-\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (1*\isoLX,1*\isoLY-\isoZ) -- ++(0,-\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (0,0) -- ++(0,-\isoZ) -- ++(7*\isoRX,7*\isoRY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (0,0)
         -- ++(0,-\isoZ) -- ++(\isoLX,\isoLY) -- ++(0,-\isoZ) -- ++(\isoLX,\isoLY)
         -- ++(0,\isoZ) -- ++(\isoLX,\isoLY) -- ++(0,-\isoZ) -- ++(\isoLX,\isoLY)
         -- ++(0,\isoZ) -- ++(\isoLX,\isoLY) -- ++(0,-\isoZ) -- ++(\isoLX,\isoLY)
         -- ++(0,\isoZ) -- ++(\isoLX,\isoLY) -- ++(0,\isoZ) -- cycle;
  \draw [fill=black!10] (0,0) -- ++(7*\isoRX,7*\isoRY) -- ++(7*\isoLX,7*\isoLY) -- ++(-7*\isoRX,-7*\isoRY) -- cycle;
  % arrows air inflow
  \draw [fill=black!85] (0.5*\isoRX,0.5*\isoRY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  \draw [fill=black!85] (2.5*\isoRX,2.5*\isoRY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  \draw [fill=black!85] (4.5*\isoRX,4.5*\isoRY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  \draw [fill=black!85] (6.5*\isoRX,6.5*\isoRY-3.5*\isoZ) -- ++(-0.6*\isoLX,-0.6*\isoLY+0.6*\isoZ) -- ++(0,-0.3*\isoZ) -- ++(-1*\isoLX,-1*\isoLY)
          -- ++(0,-0.4*\isoZ) -- ++(1*\isoLX,1*\isoLY) -- ++(0,-0.3*\isoZ) -- cycle;
  % arrows hydrogen inflow
  \draw [fill=white] (0.5*\isoLX,0.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  \draw [fill=white] (2.5*\isoLX,2.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  \draw [fill=white] (4.5*\isoLX,4.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  \draw [fill=white] (6.5*\isoLX,6.5*\isoLY-1.5*\isoZ) -- ++(-0.6*\isoRX+0.6*\isoLX,-0.6*\isoRY+0.6*\isoLY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- ++(-1*\isoRX,-1*\isoRY)
          -- ++(-0.4*\isoLX,-0.4*\isoLY) -- ++(1*\isoRX,1*\isoRY) -- ++(-0.3*\isoLX,-0.3*\isoLY) -- cycle;
  % labels
  \draw (6.8*\isoRX,6.8*\isoRY-2.2*\isoZ) -- ++(1.4,1.3) node[right,align=left,yshift=0.2cm]{anode\\[-0.1cm]diffusion layer};
  \draw (6.8*\isoRX,6.8*\isoRY-2.5*\isoZ) -- ++(1.5,0.4) node[right]{membrane};
  \draw (6.8*\isoRX,6.8*\isoRY-2.8*\isoZ) -- ++(1.6,-0.5) node[right,align=left,yshift=-0.2cm]{cathode\\[-0.1cm]diffusion layer};
  \node at (1.45,-1.35) [right]{air};
  \draw (5*\isoRX,5*\isoRY-4.8*\isoZ) -- ++(1.8,-0.8) node[right]{gas distributor};
  \draw (-3.1,-1.7) node [left]{hydrogen} -- (-2.6,-0.8);
  \draw (-5.8,2.1) node [above,align=left]{air +\\[-0.1cm]vapor} -- (-6.3,0.1);
\end{tikzpicture}
}
\hfill
\subfloat[]{
\label{fig:fuelCellGasChannel}
\includegraphics[width=6.5cm]{images/fuelcellGaschannel.jpg}
}
\label{fig:fuelCellMotivation}
\caption[Fuel cell with gas distributor and porous diffusion layer]
        {Fuel cell with gas distributor and porous diffusion layer
         (a)~schematic diagram
         (b)~left: gas distributor with included gas channel used in laboratory experiment;
             top right: porous diffusion layer,
             courtesy of University of Stuttgart, group of Gerhart Eigenberger}
\end{figure}

\subsection{Alternative approaches}
There are alternative approaches that are used to simulate a
free-flow/porous-medium-flow coupling. We present a selection of approaches,
see fig~\ref{fig:alternativeApproaches} for a schematic comparison.

\begin{figure}
\centering
\begin{tabular}{ll}
\subfloat[\hspace*{2.8cm}]{
\begin{tikzpicture}[scale=1]
  % fix width for caption
  \draw [white,<->] (3.1,-0.3) -- node[midway,right]{transition zone} ++(0,0.6);
  \begin{scope}
    \clip (0,-1.5) rectangle (3,1.5);
    \foreach \position in {
      (0,-0.2), (0.9,-0.3), (1.1,-0.5), (1.7,-0.2), (2.8,-0.3), (3.2,-0.3),
      (0.3,-1), (0.9,-1.6), (1.8,-1.1), (2,-1.1), (2.2,-1), (2.8,-1.7), (3.32,-1)
      }
    {
      \fill[darkgray] \position circle (0.4);
    }
  \end{scope}
  \draw (0,-1.5) rectangle (3,1.5);
  % labels
  \draw (3,1.5) node[below right]{Navier-Stokes};
  \draw (3,1) node[below right]{or Boltzmann};
  \draw (3.2,-0.5) node[right]{matrix} -- ++(-0.4,0.2);
\end{tikzpicture}
}
&
\subfloat[\hspace*{2.8cm}]{
\begin{tikzpicture}[scale=1]
  \fill [top color=white,bottom color=gray] (0,-0.3) rectangle (3,0.3);
  \fill [gray](0,-0.3) rectangle (3,-1.5);
  \draw (0,-1.5) rectangle (3,1.5);
  % labels
  \draw [<->] (3.1,-0.3) -- node[midway,right]{transition zone} ++(0,0.6);
  \draw (3.1,1) node[right]{Stokes};
  \draw (3.1,-1) node[right]{Darcy};
\end{tikzpicture}
}
\\
\subfloat[\hspace*{2.8cm}]{
\label{fig:alternativeApproachesOverlap}
\begin{tikzpicture}[scale=1]
  % fix width for caption
  \draw [white,<->] (3.1,-0.3) -- node[midway,right]{transition zone} ++(0,0.6);
  \fill [gray](0,-0.1) rectangle (3,-1.5);
  \fill [pattern=north east hatch,
         pattern color=gray, hatch distance=0.3cm,
         hatch thickness=0.1cm](0,-0.1) rectangle (3,0.1);
  \draw (0,-1.5) rectangle (3,1.5);
  % labels
  \draw (3.2,0) node[right]{overlap};
  \draw [<->] (3.1,-0.1) -- ++(0,1.6);
  \draw (3.2,1) node[right]{Stokes};
  \draw [<->] (3.2,0.1) -- ++(0,-1.6);
  \draw (3.2,-1) node[right]{Darcy};
\end{tikzpicture}
}
&
\subfloat[\hspace*{2.8cm}]{
\begin{tikzpicture}[scale=1]
  % fix width for caption
  \draw [white,<->] (3.1,-0.3) -- node[midway,right]{transition zone} ++(0,0.6);
  \begin{scope}
    \fill[gray] (0,0) rectangle (3,-1.5);
    \clip (0,-1.5) rectangle (3,1.5);
    \foreach \position in {
      (1,1.4), (1.6,1.3), (2,1.4), (2.4,1.25), (3,1.5),
      (1.3,1), (1.7,0.85), (2.1,1), (2.8,1.1),
      (1.3,0.55), (2.2,0.6), (2.65,0.7), (3,0.5),
      (1.25,0.1), (1.7,0.35), (2.05,0.15), (2.5,0.3), (2.9,0.05),
      (1.6,-0.1), (2,-0.25), (2.4,-0.1), (2.8,-0.35),
      (0.8,-0.6), (1.4,-0.5), (2.2,-0.6), (2.55,-0.8), (3.1,-0.7),
      (0.3,-1.1), (0.8,-1.05), (1.2,-0.9), (1.7,-0.85), (2.1,-1), (2.8,-1.1),
      (0,-1.4), (0.5,-1.6), (1,-1.4), (1.6,-1.3), (2,-1.4), (2.4,-1.35), (3,-1.5)
      }
    {
      \draw \position circle (0.2);
    }
    \foreach \position in {
      (0,1.4), (0.6,1.5),
      (0.3,1.1), (0.7,1.05),
      (-0.1,0.8), (0.4, 0.7), (0.9,0.7),
      (-0.15,0.4), (0.35,0.2), (0.85,0.3),
      (0.1,-0.2), (0.5,-0.3), (0.9,-0.1),
      (-0.1,-0.8), (0.4,- 0.7)
      }
    {
      \draw [lightgray] \position circle (0.2);
    }
  \end{scope}
  \draw (0,-1.5) rectangle (3,1.5);
\end{tikzpicture}
}
\end{tabular}
\label{fig:alternativeApproaches}
\caption[Alternative coupling approaches]
        {Alternative coupling approaches
        (a)~pore-scale simulation
        (b)~Brinkman
        (c)~interface control domain decomposition
        (d)~smoothed particle hydrodynamics}
\end{figure}

The most straight forward method is a pore-scale simulation of the whole system without any
coupling condition. This can be done with a Navier-Stokes discretization
as in \citet{chandesris2013} or by solving the discrete Boltzmann equation with a
lattice Boltzmann method \citep{krafczyk2014}. For both variants, the pore geometry
must be resolved which requires a fine grid and leads to a high computational demand.
The pore geometry must be either simple or obtained from a computer tomography scan. These
dependencies limit the domain size to small lab samples. Two-phase flow computations
exacerbate the problem with the computational costs. Pore-scale simulations are
often used to evaluate coupling concepts \citep{fattahi2016} or to obtain effective
parameters for upscaled models \citep{hueppe2011}.

The Brinkman equation \citep{brinkman1949} combines Darcy flow and Stokes flow. A
parameter blends the portions of both equations. This leads to a transition zone
between the free and the porous-medium flow. In numerical simulations, the transition
zone must maintain a certain thickness. \citet{krotkiewski2015} use the Brinkman
equation to simulate a fractured porous medium obtained from a computer tomography
scan. A comparison between an implementation using a Beavers-Joseph-type coupling
and an implementation using a transition layer modeled by a Brinkman domain shows
good agreement when the transition layers are small and have low permeabilities
\citep{nield2009a}. The Brinkman equation is easy to implement and is used in
commercial codes \citep{cimolin2013}. The application of the Brinkman equation for
larger domains is challenged by \citet{nield2009b}. We are not aware of any work
extending the Brinkman model to two-phase flows in the porous medium.

The interface control domain decomposition (ICDD) method uses overlapping subdomains.
It is a recent idea, presented by \citet{discacciati2016}, to get rid of the
Beavers-Joseph condition. In the overlapping region, the Stokes and the Darcy flow are
present. This is physically justified, as the free flow enters the porous medium to
some extent. The coupling is achieved by Dirichlet boundary conditions for pressure
and velocity at the ends of the overlapping domain. The gap between the overlapping
solutions is minimized. Compared to a Robin-Robin or Dirichlet-Neumann coupling,
we expect that the ICDD leads to a better converging domain decomposition method.

A different way to simulate flow would use smoothed particle hydrodynamics (SPH) methods.
These methods are not based on a mesh but represent the flow by a large number of small
particles. The particles move similar to the fluid and maintain some of the fluid’s
properties, for example, its density. \citet{shao2010} presents an interaction of water
inside and outside of a porous medium. \citet{basser2016} use SPH to simulate a water
tank with a porous bed where salt water displaces the less dense fresh water.

\section{Outline}
This chapter explains the motivation behind this work and outlines the
goals of the work at hand. Chapter two introduces
the basic physical concepts, for example, pressure, phases and porous media, and
important processes like diffusion and evaporation. The third chapter describes the
mathematical model, i.\,e., the relevant processes are modeled as
mathematical equations. Especially partial differential equations are an important
tool. In chapter four the equations are discretized to render a computational
approximation possible, and the used schemes as well as the implementation are presented.
Chapter five contains numerical results to demonstrate the capabilities of the
implementation and its applications: We will simulate a laboratory experiment
concerning soil-water evaporation, a part of a nuclear waste repository in a
geological formation, and a section of a fuel cell. The final chapter
concludes with the summary of results and an outlook.
