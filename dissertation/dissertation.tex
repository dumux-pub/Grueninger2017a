% packages needed: libertine, boondox-fonts, newtx, pgfplots
% add -shell-escape to pdflatex to generate externalized TikZ images, not for final version
\documentclass[
  a4paper,12pt,DIV=14,
  headings=normal,                          % not so huge headings
  bibliography=totoc,                       % Bibliography to table of contents
  twoside=semi                              % have same margin left and right
  ]{scrbook}
\usepackage[utf8]{inputenc}                 % use modern input encoding
\usepackage[T1]{fontenc}                    % use 8 bit font for German umlauts and Polish letters
\usepackage[ngerman,english]{babel}         % languages: German for Kurzfassung
\usepackage[intlimits]{amsmath}             % basic math stuff
\usepackage{amssymb,amsfonts}               % more basic math stuff
\usepackage{booktabs}                       % nice tables
\usepackage{graphicx}                       % include images
\usepackage{ifthen}                         % have basic branching, used for nomgroups
\usepackage{lettrine}                       % initial letter for chapters
\usepackage[mono=false]{libertine}          % use font Linux Libertine
\usepackage[protrusion=true,expansion=true]
           {microtype}                      % improved typography
\usepackage[libertine]{newtxmath}           % use math font matching to Libertine
\usepackage{natbib}                         % nice cite style
\usepackage[refpage,intoc]{nomencl}         % nomenclature
\usepackage[figuresright]{rotating}         % have figures sideways, bottom to the right
\usepackage[caption=false]{subfig}          % place pictures side-by-side and number them
\usepackage{tikz}                           % draw with LaTeX commands -- finally, sigh
\usetikzlibrary{decorations.pathmorphing,patterns,spy} % external, removed for final version
\usepackage{pdfpages}                       % include pages from external PDF
\usepackage{pgfplots}                       % for beautifully drawn plots
\pgfplotsset{compat=1.8}
\usepackage{placeins}                       % provides \FloatBarrier preventing figures in following (sub)section
%\usepackage[colorinlistoftodos]{todonotes}  % nice To-do boxes
\usepackage{units}                          % set units automatically, includes nicefrac
\usepackage{xspace}                         % spaces after command
\usepackage[colorlinks=false, pdfborder={0 0 0}, plainpages=false,
            bookmarksopen=true, pdfusetitle]{hyperref} % PDF hyperlinks
\usepackage[all]{hypcap}                    % links to images point to image and not caption

%%%
% document options
%%%
\providecommand{\useosf}{} % backwards-compatibility for old packages not providing \useosf
\useosf                                     % old-style figures everywhere except in math mode
\linespread{1.05}                           % compensate for large DIV value
\setlength{\nomitemsep}{-\parsep}           % no extra spacing between nomenclature entries
\setcounter{tocdepth}{1}                    % don't list subsubsections
\def\table{\def\figurename{Table}\figure}   % tables are numbered consecutive among figures
\let\endtable\endfigure                     % dito
\pagestyle{plain}                           % don't use headings
% dictum options
\renewcommand*{\dictumauthorformat}[1]{#1}  % dictum author without parentheses
\renewcommand*{\dictumwidth}{.55\textwidth} % set width of dictum
\addtokomafont{dictumtext}{\footnotesize}   % use smaller font
\setkomafont{dictumauthor}{\usekomafont{dictumtext}}  % use same font for text and author
\renewcommand*{\chapterheadstartvskip}{\vspace*{3.73\baselineskip}} % move chapter head to create space for dictum

%%%
% self-defined macros
%%%
\newcommand*{\diff}{\mathop{}\!\mathrm{d}}  % differential d for integrals and differentiation
\newcommand*{\partialfrac}[1]{\frac{\partial}{\partial #1}} % partial derivative
\newcommand*{\partialfracTwo}[1]{\frac{\partial^2}{\partial #1^2}} % second partial derivative

\DeclareMathOperator*{\divergence}{div}
\DeclareMathOperator*{\gradient}{grad}
\newcommand\define{\mathrel{\mathop:\!\!=}}

\newcommand{\Dune}{\textsc{Dune}\xspace}
\newcommand{\Dumux}{DuMu$^\text{x}$\xspace}

% typeset chemical formulas
\newcommand*\chem[1]{\ensuremath{\mathrm{#1}}}

% unit for nomenclature at very right
\newcommand{\nomunit}[1]{\renewcommand{\nomentryend}{\hspace*{\fill}$\unit{#1}$}}
\newcommand{\nomunitfrac}[2]{\renewcommand{\nomentryend}{\hspace*{\fill}$\unitfrac{#1}{#2}$}}
% link to page from nomenclature
\renewcommand*{\pagedeclaration}[1]{\unskip, p. \hyperpage{#1}}

% macros to add chapter heading in table of figures, taken from TeXnische Komödie 3/2015
\makeatletter
% write to aux file, whether chapter contains a floating at all
\let\chapterhas@original@addcontentsline\addcontentsline
\renewcommand*{\addcontentsline}[1]{%
  \immediate\write\@auxout{\string\chapterhas{\thechapter}{#1}}%
  \chapterhas@original@addcontentsline{#1}%
}
% command to create list of figures
\newcommand*{\chapterhas}[2]{%
  \global\@namedef{chapterhas@#1@#2}{true}%
}
% macro to add chapter heading
\renewcommand*{\addchaptertocentry}[2]{%
  \addtocentrydefault{chapter}{#1}{#2}%
  \if@chaptertolists
    \doforeachtocfile{%
      \iftocfeature{\@currext}{chapteratlist}{%
        \ifundefinedorrelax{chapterhas@\thechapter @\@currext}{%
        }{%
          \addxcontentsline{\@currext}{chapteratlist}[{#1}]{#2}%
        }%
      }{}%
    }%
    \@ifundefined{float@addtolists}{}{\scr@float@addtolists@warning}%
  \fi
}
\makeatother

% force axis plot and labels on top of data
\makeatletter \newcommand{\pgfplotsdrawaxis}{\pgfplots@draw@axis} \makeatother
\pgfplotsset{axis line on top/.style={
  axis line style=transparent,
  ticklabel style=transparent,
  tick style=transparent,
  axis on top=false,
  after end axis/.append code={
    \pgfplotsset{axis line style=opaque,
      ticklabel style=opaque,
      tick style=opaque,
      grid=none}
    \pgfplotsdrawaxis}
  }
}


% externalize tikz images to speed up compilation, not for final version
% \tikzexternalize[prefix=tikz-externals/]
% \makeatletter
% \renewcommand{\todo}[2][]{\tikzexternaldisable\@todo[#1]{#2}\tikzexternalenable}
% \makeatother

% make DOIs in bibliography clickable / linked to doi URL
\DeclareUrlCommand\doi{\def\UrlLeft##1\UrlRight{doi:\nobreakspace\href{http://dx.doi.org/##1}{##1}}\urlstyle{rm}}

% costum TikZ hatch pattern
\makeatletter
\tikzset{hatch distance/.store in=\hatchdistance,
         hatch distance=5pt,
         hatch thickness/.store in=\hatchthickness,
         hatch thickness=5pt}
\pgfdeclarepatternformonly[\hatchdistance,\hatchthickness]{north east hatch}% name
    {\pgfqpoint{-1pt}{-1pt}}% below left
    {\pgfqpoint{\hatchdistance}{\hatchdistance}}% above right
    {\pgfpoint{\hatchdistance-1pt}{\hatchdistance-1pt}}%
    {\pgfsetcolor{\tikz@pattern@color}
     \pgfsetlinewidth{\hatchthickness}
     \pgfpathmoveto{\pgfqpoint{0pt}{0pt}}
     \pgfpathlineto{\pgfqpoint{\hatchdistance}{\hatchdistance}}
     \pgfusepath{stroke}}
\makeatother

% define colors from Viridis palette
\definecolor{viridisA}{rgb}{0.26700401,0.00487433,0.32941519}
\definecolor{viridisB}{rgb}{0.28262297,0.14092556,0.45751726}
\definecolor{viridisC}{rgb}{0.25564519,0.26070284,0.52831152}
\definecolor{viridisD}{rgb}{0.20862342,0.36775151,0.55267486}
\definecolor{viridisE}{rgb}{0.1666171 ,0.46370813,0.55811913}
\definecolor{viridisF}{rgb}{0.13117249,0.55589872,0.55245948}
\definecolor{viridisG}{rgb}{0.12808703,0.64774881,0.52349092}
\definecolor{viridisH}{rgb}{0.2393739 ,0.73558828,0.45568838}
\definecolor{viridisI}{rgb}{0.44013691,0.81113836,0.3409673}
\definecolor{viridisJ}{rgb}{0.68894351,0.86544779,0.18272455}

% start collecting data for nomenclature
\makenomenclature
% text after nomenclature
\renewcommand{\nompostamble}{When symbols are only used in close proximity to their definition, they have been omitted from the nomenclature.}
\renewcommand{\nomgroup}[1]{%
  \ifthenelse{\equal{#1}{Z}}{\item}{%
    \ifthenelse{\equal{#1}{Y}}{\item}}}

%%%
% opening
%%%
\pagenumbering{roman}
\KOMAoption{cleardoublepage}{empty}         % don't show numbers on empty pages

%%%
% Title Page
%%%
\title{Numerical Coupling of Navier-Stokes and Darcy Flow for Soil-Water Evaporation}
\author{Christoph Grüninger}

\begin{document}

% include cover
% \tikzexternaldisable
\includepdf[pages={1},scale=1]{images/cover/cover253.pdf}
% \tikzexternalenable
\setcounter{page}{0}

\begin{titlepage}
\begin{center}
{\normalfont\sffamily
\vspace*{\fill}
{\Large\bfseries
Numerical Coupling of Navier-Stokes and Darcy Flow\\
for Soil-Water Evaporation
}
\vfill
von der Fakultät Bau- und Umweltingenieurwissenschaften\\
und dem Stuttgart Research Centre for Simulation Technology\\
der Universität Stuttgart \\
zur Erlangung der Würde eines Doktor-Ingenieurs (Dr.-Ing.)\\
genehmigte Abhandlung\\
\vfill
vorgelegt von \\
\bigskip
{\Large Christoph Oskar Grüninger} \\
\bigskip
aus Göppingen\\
\vfill
\begin{tabular}{@{}ll@{}}
Hauptberichter: & apl. Prof.\;Dr.\;rer.\;nat. Bernd Flemisch \\
Mitberichter: &  Prof.\;Dr.-Ing. Rainer Helmig \\
              &  Prof.\;Béatrice Rivière, Ph.\,D., Rice University, Houston, TX \\[3ex]
\multicolumn{2}{@{}l@{}}{Tag der mündlichen Prüfung: 18. Mai 2017} \\
\end{tabular}
\vfill
Institut für Wasser- und Umweltsystemmodellierung \\
der Universität Stuttgart \\
\bigskip
2017
}
\end{center}
\end{titlepage}

% license notice, lower title back
\pagebreak
\null\vfill
\begin{minipage}{0.8\textwidth}
\centering
\begin{minipage}[b]{0.96\textwidth}
\footnotesize
This work is distributed under the terms of the
Creative Commons Attribution 4.0 International License
(CC\,BY 4.0)
\url{https://creativecommons.org/licenses/by/4.0/}
which permits unrestricted use, modification, distribution, and reproduction in any
medium, provided you give appropriate credit to the original author and the source,
provide a link to the Creative Commons license, and indicate whether changes were made.
Note the differing copyright for figure~\ref{fig:zurichWindchannel}, requiring proper
citing of \citet{mosthafHelmigOr2014}.
\end{minipage}
\\
\vspace*{0.4cm}
\begin{minipage}{1.7cm}
\include{images/cc-by}
\end{minipage}
\end{minipage}

% bibliographic information
% \tikzexternaldisable
\includepdf[pages={5-6},scale=1]{images/cover/cover253.pdf}
% \tikzexternalenable

\include{0_abstract}

\cleardoublepage
\phantomsection\pdfbookmark{Contents}{contents}
\tableofcontents
\KOMAoptions{open=left}
\clearpage
\phantomsection\pdfbookmark{Figures and Tables}{figuresandtables}
\listoffigures
\KOMAoptions{open=right}

% additional nomenclature entries
\nomenclature{$F$}{force\nomunitfrac{kg\,m}{s^2}\nomnorefpage}
\nomenclature{$m$}{mass\nomunit{kg}\nomnorefpage}
\nomenclature[a$O$]{$\mathcal{O}$}{big O, upper bound of asymptotic growth rate\nomnorefpage}
\nomenclature{$t$}{time\nomunit{s}\nomnorefpage}
\nomenclature{$x$}{space coordinate $x = \left(x_1, x_2 , \dots\right)^\intercal$\nomunit{m}\nomnorefpage}
\nomenclature[z1]{$\divergence$}{divergence, for matrices $\divergence(U)_i \define \sum_j \partialfrac{x_j}U_{i,j}$\nomnorefpage}
\nomenclature[z2]{$\gradient$}{gradient, for vectors $\gradient(v)_{i,j} \define \gradient\left(v_i\right)_j$\nomnorefpage}
\nomenclature[z3]{ff, pm}{free flow, porous medium subdomain superscript\nomnorefpage}
\nomenclature[z4]{a, w}{air, water component superscript\nomnorefpage}
\nomenclature[z5]{g, l, s}{gas, liquid, solid phase subscript\nomnorefpage}

%%%
% document contents
%%%
\cleardoublepage
\pagenumbering{arabic}
\KOMAoption{cleardoublepage}{plain}         % empty pages show only page numbers

\include{1_introduction}
\include{2_fundamentals}
\include{3_modeling}
\include{4_discretization}
\include{5_results}
\include{6_finale}

\include{8_colophon}
\include{x_danksagung}

% indices
\KOMAoptions{open=left}
\printnomenclature
\KOMAoptions{open=right}
\cleardoublepage
\bibliographystyle{abbrvnat}
\bibliography{bibliography}
\nocite{wikipedia}

% include other issues of series
\KOMAoption{cleardoublepage}{empty}
\cleardoublepage
% \tikzexternaldisable
\includepdf[pages={1-}]{images/cover/listold253.pdf}
% \tikzexternalenable

\end{document}
