% calculate data for chapter 3, image related to REV
function averaging()
  % x values
  steps = [0.1:0.05:0.4,0.4:0.025:3,3:0.075:10];
  % both y values
  porosity = zeros(length(steps),2);

  % output number of total steps
  disp(length(steps));

  % calculate smallest rectangle
  porosity(1,1) = dblquad(@chiPoreCenter,0,steps(1),0,steps(1),10);
  porosity(1,2) = dblquad(@chiPoreOuter,0,steps(1),0,steps(1),10);

  % calculate additional parts not yet calculated
  for i = 2:length(steps)
    % output current step
    disp(i);
    fflush(stdout);
    porosity(i,1) = porosity(i - 1,1) ...
                    + dblquad(@chiPoreCenter,0,steps(i),steps(i-1),steps(i),10) ...
                    + dblquad(@chiPoreCenter,steps(i-1),steps(i),0,steps(i-1),10);
    porosity(i,2) = porosity(i - 1,2) ...
                    + dblquad(@chiPoreOuter,0,steps(i),steps(i-1),steps(i),10) ...
                    + dblquad(@chiPoreOuter,steps(i-1),steps(i),0,steps(i-1),10);
  end;

  % divide pore space by total space to obtain porosity
  for i=1:length(steps)
    porosity(i,:) = porosity(i,:) / steps(i)^2;
  end;

  % write results to file
  outputFile = fopen('porosity.dat','w');
  fprintf(outputFile,'size\tcenter\touter\n');
  fprintf(outputFile,'0\t0\t0\n');
  for i=1:length(steps)
    fprintf(outputFile,'%d\t%d\t%d\n',2*steps(i),porosity(i,1),porosity(i,2));
  end;
  fclose(outputFile);

  % plot results
  plot(2*steps,porosity(:,1),'b-',2*steps,porosity(:,2),'r-');
end

% characteristic function for pores
% circles have a diameter of 1 and the center is located
% at every integer coordinate
function matrix = chiPoreCenter(x,y)
  matrix = max(0,
           1 - 1.0 * ((x - floor(x)).^2 + (y - floor(y)).^2 < 0.25) ...
           - 1.0 * ((x - ceil(x)).^2 + (y - floor(y)).^2 < 0.25) ...
           - 1.0 * ((x - floor(x)).^2 + (y - ceil(y)).^2 < 0.25) ...
           - 1.0 * ((x - ceil(x)).^2 + (y - ceil(y)).^2 < 0.25));
end

% move cirlces by half circle in x direction
function matrix = chiPoreOuter(x,y)
  matrix = chiPoreCenter(x+0.5,y);
end
