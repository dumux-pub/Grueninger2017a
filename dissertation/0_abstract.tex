\cleardoublepage
\phantomsection\pdfbookmark{Abstract}{abstract}
\chapter*{Abstract}
The objective of this work is to develop algorithms and provide a framework for an efficient coupling
of free flow and porous-medium flow to simulate porous-medium-soil-water evaporation.
The implementation must particularly be capable
of simulating laminar free flows, be fast enough for applied research, and cover
simulations in two and three dimensions with complex geometries.

\paragraph{General}
We introduce a model for a compositional non-isothermal free flow coupled with a two-fluid-phase
compositional non-isothermal porous-medium flow. The free flow is modeled with the
Navier-Stokes, component and energy transport equations. The porous-medium flow is
modeled with compositional two-fluid-phase Darcy and energy transport equations. As the
pressure has different orders in the free-flow and porous-medium-flow subdomains, the
coupling is not straightforward. Although the simulation of the coupled flows is
motivated by a laboratory experiment to measure soil-water evaporation caused by
wind blowing over a water-filled porous bed, we intend to also explore its use
in other applications

\paragraph{Conceptual model}
The free flow is considered to be incompressible and laminar. We also assume that air
and water follow nonlinear laws that describe their physical properties, and binary diffusion.
Within the porous
medium only creeping flows occur. Many quantities are averaged and used in a macroscopic
sense. We use a formulation of two-phase Darcy law using the liquid saturation and the gas
pressure as primary variables. The component mass fractions are calculated by Henry's
law and the vapor pressure. The liquid phase may locally vanish leading to a variable switch,
where the vapor mass fraction is tracked instead of the liquid saturation. We assume that a local thermodynamic
equilibrium is valid everywhere within the domain, even across the interface. We follow
the coupling concept proposed by \citet{mosthaf2011}, including the
Beavers-Joseph-Saffman approach which has a sharp interface between the two subdomains.

\paragraph{Discretization}
We use a cell-centered finite volume method (FVM) on an axially parallel grid to discretize
the partial differential equations of the compositional two-phase Darcy's law, the heat equation in
both subdomains, and the component transport in the free-flow domain. For the Navier-Stokes
equation, we use the marker and cell (MAC) scheme which moves the degrees of freedom for
the velocities towards the edges of the grid elements, forming one secondary, staggered grid
per dimension. The MAC scheme is stable and can be interpreted as a FVM.
The coupling conditions are applied without additional variables along the
coupling interface. They are incorporated as Dirichlet, Neumann or Robin boundary conditions
resulting in interface fluxes.

\paragraph{Implementation}
For the porous-medium flow, we use the finite-volume implementation provided by \Dumux. The
marker and cell scheme is implemented on top of \Dune-PDELab utilizing the material laws
from \Dumux. The grid is split into two subdomains and the grid elements can be graded.
This is especially useful for developing smaller elements closer to the interface.
We can use complex geometries in two or three dimensions. The coupling is provided by a
\Dune-Multidomain local coupling operator. The time integration is approximated with an
implicit Euler scheme and an adaptive time stepping. The system of nonlinear equations
is linearized by a Newton method. All contributions to the Jacobian are compiled in one
system of linear equations.

\paragraph{Solving the linear system}
The resulting matrices are difficult to solve. Although they are sparse, with a blocked
structure of bands of nonzero entries, the matrices contain a saddle point problem and
are nonsymmetric. We solve the matrices with direct methods. We also investigate iterative methods to get around
the computational complexity and memory consumption of the direct methods: An Algebraic Multigrid
(AMG) method, a Schur complement method, and a Generalized Minimal Residual method (GMRES)
preconditioned with the reordering algorithm MC64 and an incomplete LU factorization
with threshold and pivoting (ILUTP). We experience problems with AMG's error criteria
leading to convergence problems. The Schur complement method is slow, as the Schur complement,
which is not explicitly calculated, lacks preconditioners. GMRES with ILUTP shows
similar results to a direct method, but reveals a restriction on the time step size for larger
problem sizes, flawing a possible speedup compared to the direct methods.

\paragraph{Numerical results}
We validate our implementation for proper operation with the simulation of a laboratory experiment
for soil-water evaporation. The laboratory experiment consists of a water-filled sand box
with a horizontal pipe installed on top of the box and a propeller creating a constant air flow.
We use the implementation to investigate
the influence of the Reynolds number on the evaporation rate. Further, we compare the
two-dimensional simplification to different three-dimensional geometries with regard to the effects on the evaporation.
For low Reynolds numbers, the geometry of the free-flow subdomain has a significant influence
on the evaporation rate.

Another application involves a geological repository for nuclear waste. We investigate the water
saturation in the concrete ceiling and the rock above a ventilation gallery. Our results conclude
that within the first 200~years, only part of the concrete will dry, and the rock will remain
unaffected. This confirms the same result by another group, though they observe evaporation rates
up to the factor of ten higher.

Our third application is the water management within a polymer electrolyte membrane (PEM) fuel cell.
Neglecting electrochemistry, we simulate the flow through the gas channels and the porous layer
covering the membrane, including the transport of vapor and liquid water, the evaporation
of water within the porous layer, and how energy and vapor are conveyed away. In comparison to
the above applications, the gas phase flow is not horizontally parallel to the porous bed,
but is forced to completely enter the porous medium and leave it through a second gas channel.
We also briefly compare two different gas channel layouts.

\paragraph{Summary}
We introduce the discretization of the coupling concept and its implementation. We conduct
simulations of applications from different areas. We show the versatility of our approach and
that it can be used as the basis for further research.

\cleardoublepage
\phantomsection\pdfbookmark{German abstract}{germanabstract}
\setchapterpreamble[or]{German abstract}
\chapter*{Kurzfassung}
\foreignlanguage{ngerman}{Ziel der vorliegenden Arbeit ist die Entwicklung von Algorithmen
und das Bereitstellen eines Frameworks für die effiziente
Kopplung zwischen einer freien Strömung und einer Strömung in einem porösen Medium um
Bodenwasserverdunstung zu simulieren. Insbesondere muss die Software in der
Lage sein laminare freie Strömung zu simulieren, effizient genug sein für Anwendungen
und zwei- wie drei-dimensionale Simulationen mit komplexen Geometrien abdecken.}

\paragraph{Überblick}
\foreignlanguage{ngerman}{Wir stellen ein Kopplungsmodell für eine nichtisotherme freie
Strömung und eine nichtisotherme Zwei-Flüssigphasen-Strömung im
porösen Medium, jeweils mit Komponententransport, vor. Die freie Strömung ist mit der
Navier-Stokes-, einer Komponenten- und einer Energietransport-Gleichung modelliert, die
Strömung im porösen Medium mit einer Darcy-Gleichung für zwei Flüssigphasen und zwei Komponenten
und einer Energietransport-Gleichung. Die Kopplung wird erschwert durch den Druck, der
in den Teilgebieten von unterschiedlicher Ordnung ist. Die Simulation der gekoppelten
Strömungen ist durch einen Laborversuch zu Bodenwasserverdunstung motiviert, aber auch andere
Anwendungen wollen wir untersuchen können.}

\paragraph{Modellkonzept}
\foreignlanguage{ngerman}{Die freie Strömung sei inkompressibel
und laminar, die physikalischen Fluid-Eigenschaften folgen nichtlinearen
Gesetzen. Wir beschränken uns auf binäre Diffusion und im porösen Medium gibt es
nur schleichende Strömung. Viele Größen sind gemittelt und werden makroskopisch
verwendet. Die Zwei-Phasen-Darcy-Strömung hat die Primärvariablen Sättigung
der Flüssigphase und Gasphasen-Druck. Die Massenbrüche der Komponenten werden durch
das Henry-Gesetz und den Dampfdruck beschrieben. Stellenweise kann die Flüssigphase
verschwinden, dann wird die Primärvariable von der Wassersättigung zum
Dampfmassenbruch verändert. Es gelte ein lokales thermodynamisches Gleichgewicht,
auch über das Kopplungsinterface hinweg. Wir übernehmen das Kopplungskonzept von
\citet{mosthaf2011}, das auf dem Beavers-Joseph-Saffman-Ansatz mit einem abrupten
Interface zwischen den Teilgebieten beruht.}

\paragraph{Diskretisierung}
\foreignlanguage{ngerman}{Wir diskretisieren mit einer zell-zentrierten Finite-Volumen-Methode
(FVM) auf einem achsenparallelen Gitter alle partiellen Differentialgleichungen
außer der Navier-Stokes-Gleichung. Für letztere setzen wir die
Marker-and-Cell-Methode (MAC) ein, welche die
Geschwindigkeitsfreiheitsgrade auf die Elementkanten verschiebt und ein versetztes
Sekundärgitter pro Dimension bildet. Die MAC-Methode ist stabil und kann als FVM
aufgefasst werden. Die Kopplungsbedingungen werden ohne zusätzliche Variablen auf
dem Interface als Dirichlet-, Neumann- oder Robin-Randbedingungen angewandt,
welche Flüsse über das Interface ergeben.}

\paragraph{Implementierung}
\foreignlanguage{ngerman}{Für die Strömung im porösen Medium kommt die FVM-Implementierung
von \Dumux zum Einsatz. MAC wird mit Hilfe von \Dune-PDELab und den Materialgesetzen von
\Dumux umgesetzt. Das Gitter wird in zwei Teilgebiete zerlegt, Gitterelemente können
stufenweise verkleinert werden, was für kleinere Zellen in Richtung der Kopplung
verwendet wird. Außerdem können sowohl zwei- wie drei-dimensionale komplexe Geometrien umgesetzt
werden. Die Kopplung wird mit einem lokalen Operator auf Basis von \Dune-Multidomain
durchgeführt. Die Zeitintegration wird mit einem impliziten Euler-Verfahren genähert.
Wir linearisieren das System nichtlinearer Gleichungen mit einem Newton-Verfahren.
Alle Beiträge für die Jacobimatrix werden in einem System linearer Gleichungen
zusammengetragen.}

\paragraph{Lösen des linearen Systems}
\foreignlanguage{ngerman}{Die resultierenden Matrizen sind schwierig zu lösen. Sie sind
dünnbesetzt mit einer geblockten Bandstruktur der Nicht-Null-Einträge. Allerdings enthalten sie
ein Sattelpunktproblem und sind unsymmetrisch. Wir lösen sie mit direkten Verfahren,
die eine hohe Rechenzeit- und Speicherkomplexität aufweisen. Wir untersuchen alternativ
iterative Verfahren: ein algebraisches Mehrgitterverfahren (AMG), ein Schurkomplement-Verfahren
und ein GMRES-Verfahren (Generalized Minimal Residual), vorkonditioniert mit einer
Umsortierung nach dem MC64-Algorithmus und einer unvollständigen LU-Zerlegung mit
Schwellwert und Pivotisierung (ILUTP). Wir stellen Konvergenzprobleme durch Unstimmigkeiten
beim Fehlerkriterium des AMGs fest. Das Schurkomplement-Verfahren ist langsam, weil ein geeigneter
Vorkonditionierer für das nicht explizite Schurkomplement fehlt. GMRES mit ILUTP erzielt
ähnliche Ergebnisse wie die direkten Verfahren, zeigt aber eine Beschränkung der
Zeitschrittweite für größere Probleme, was mögliche Beschleunigungen zunichtemacht.}

\paragraph{Numerische Ergebnisse}
\foreignlanguage{ngerman}{Wir überprüfen unsere Implementierung mit der Simulation eines Laborexperiments zur
Bodenwasserverdunstung. Der Versuchsaufbau ist eine wassergefüllte Sandbox mit einer
horizontal darüber verlaufenden Röhre mit konstanter Luftströmung.
Wir untersuchen den Einfluss der Reynolds-Zahl auf die Verdunstungsrate und vergleichen
zweidimensionale Vereinfachungen mit verschiedenen dreidimensionalen Geometrien.
Für moderate Reynolds-Zahlen hat die Geometrie des Gebiets der freien Strömung einen
signifikanten Einfluss.}

\foreignlanguage{ngerman}{Eine andere Anwendung ist ein atomares Endlager in geologischen
Formationen. Untersucht wird die Wassersättigung in der Betondecke und dem darüber
liegenden Deckgestein eines Lüftungsschachts. Innerhalb der ersten $\unit[200]{Jahre}$
trocknet der Beton nur teilweise und das Gestein bleibt unverändert.
Dies stimmt mit dem Ergebnis einer anderen Forschergruppe überein, die allerdings
Verdunstungsraten erzielen, die von unseren bis zu einem Faktor zehn abweichen.}

\foreignlanguage{ngerman}{Unsere dritte Anwendung ist das Wassermanagement einer
Polymer-Elektrolyt-Membran-Brennstoffzelle (PEM). Unter Vernachlässigung der
Elektrochemie simulieren wir die Strömung durch die Gaskanäle und die poröse
Schicht um die Membran unter Berücksichtigung des Transports von Dampf und
flüssigem Wasser, der Verdunstung in der porösen Schicht und des Abtransports
von Energie und Dampf. Die Strömung findet nicht nur horizontal über die
poröse Schicht statt, sondern muss vollständig ins poröse Medium eindringen,
um einen zweiten Gaskanal zu erreichen. Wir vergleichen auch zwei unterschiedliche
Anordnungen der Gaskanäle.}

\paragraph{Zusammenfassung}
\foreignlanguage{ngerman}{Wir stellen die Diskretisierung des Kopplungskonzepts und
dessen Umsetzung vor. Wir simulieren Anwendungen aus unterschiedlichen Bereichen.
Dabei zeigen wir die Vielseitigkeit unseres Ansatzes und dass dieser für weitere
Forschungen verwendet werden kann.}
