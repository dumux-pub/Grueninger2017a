// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_VENTILATION_GALLERY_PROBLEM_HH
#define DUMUX_VENTILATION_GALLERY_PROBLEM_HH

#include <dumux/material/fluidsystems/h2oair.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/problem.hh>

#include "ventilationgallerydarcysubproblem.hh"
#include "ventilationgallerystokessubproblem.hh"

namespace Dumux
{

template <class TypeTag>
class VentilationGallery;

template <class TypeTag> class MultiDomainProblem;

namespace Properties
{
// problems
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, Problem,
              Dumux::VentilationGallery<TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>);
SET_TYPE_PROP(StokesSubProblem, Problem,
              Dumux::VentilationGalleryStokesSubProblem<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(DarcySubProblem, Problem,
              Dumux::VentilationGalleryDarcySubProblem<TTAG(DarcySubProblem)>);

#ifdef USE_SIMPLE_PHYSICS
// Set the fluid system to use simple relations but complex tabulated Fluid system
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                  TabulatedComponent<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     H2O<typename GET_PROP_TYPE(TypeTag, Scalar)>>,
                                     false>);
#else
// Set the fluid system to use complex relations (default) and tabulated complex Fluid system
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                  TabulatedComponent<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     H2O<typename GET_PROP_TYPE(TypeTag, Scalar)>>>);
#endif

SET_TYPE_PROP(DarcySubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), FluidSystem));
SET_TYPE_PROP(StokesSubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), FluidSystem));

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, UseMoles, false);
SET_BOOL_PROP(DarcySubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), UseMoles));
SET_BOOL_PROP(StokesSubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), UseMoles));

// Set the output frequency
NEW_PROP_TAG(OutputFreqOutput);
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, OutputFreqOutput, 5);

// Set the default episode length
NEW_PROP_TAG(TimeManagerEpisodeLength);
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, TimeManagerEpisodeLength, 360);
}

template <class TypeTag = TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>
class VentilationGallery
: public MultiDomainProblem<TypeTag>
{
    using ParentType = MultiDomainProblem<TypeTag>;

    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using SubDomainGridView = typename GET_PROP_TYPE(TypeTag, SubDomainGridView);
    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    enum { dim = MultiDomainGridView::dimension };
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;

    constexpr static unsigned int stokesSubDomainIdx = MultiDomainIndices::stokesSubDomainIdx;
    constexpr static unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief Base class for the multi domain problem
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The GridView
     */
    VentilationGallery(TimeManager &timeManager,
                  GridView gridView)
    : ParentType(timeManager, gridView)
      , eps_(1e-8)
    {
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);

        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Vtk, FreqOutput);
        episodeLength_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
        this->timeManager().startNextEpisode(episodeLength_);
    }

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    {
        this->timeManager().startNextEpisode(episodeLength_);
    }

    //! \copydoc Dumux::CoupledProblem::shouldWriteOutput()
    bool shouldWriteOutput() const
    {
        return this->timeManager().timeStepIndex() % freqOutput_ == 0
               || this->timeManager().episodeWillBeFinished()
               || this->timeManager().willBeFinished();
    }

    /*!
     * \brief Initialization multi-domain and the sub-domain grids
     */
    void initializeGrid()
    {
        Scalar interfaceVerticalPos = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);
        this->mdGrid().startSubDomainMarking();

        for (auto eIt = this->mdGrid().template leafbegin<0>();
              eIt != this->mdGrid().template leafend<0>(); ++eIt)
        {
            auto globalPos = eIt->geometry().center();
            // only for interior entities, required for parallelization
            if (eIt->partitionType() == Dune::InteriorEntity)
            {
                if (globalPos[dim - 1] > interfaceVerticalPos)
                {
                    this->mdGrid().addToSubDomain(darcySubDomainIdx, *eIt);
                }
                else
                {
                    this->mdGrid().addToSubDomain(stokesSubDomainIdx, *eIt);
                }
            }
        }
        this->mdGrid().preUpdateSubDomains();
        this->mdGrid().updateSubDomains();
        this->mdGrid().postUpdateSubDomains();

        this->darcyElementIndices_.resize(this->sdGridViewDarcy().size(0));
        Dune::MultipleCodimMultipleGeomTypeMapper<MultiDomainGridView, Dune::MCMGElementLayout>
            multidomainDofMapper(this->mdGridView());
        Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
            subdomainDofMapper(this->sdGridViewDarcy());
        for (auto eIt = this->sdGridDarcy().template leafbegin<0>();
              eIt != this->sdGridDarcy().template leafend<0>(); ++eIt)
        {
            this->darcyElementIndices_[subdomainDofMapper.index(*eIt)] =
                multidomainDofMapper.index(this->mdGrid().multiDomainEntity(*eIt));
        }
    }

private:
    Scalar eps_;
    unsigned int freqOutput_;
    Scalar episodeLength_;
};

} // end namespace Dumux

#endif // DUMUX_VENTILATION_GALLERY_PROBLEM_HH
