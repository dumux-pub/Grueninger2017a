#!/bin/bash
umask 022

# check for valid arugument
if [ -z "$1" ]; then
  echo "No argument supplied, argument 1 must be the build directory"
  exit 2
fi

# custom input and output folder (adapt to your needs)
builddir=$1
outdir=$builddir/../results/

# predefined names
executable=test_evaporationpipe
input=test_simplepipe2d.input
sourcedir=$builddir/appl/multidomain/promogruenich/

# make executable
cd $sourcedir
make $executable

# refinement level: 0
echo ""
echo "refinement level: 0"
echo ""
simdir=$outdir/linearsolver/2dumfpack0

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -TimeManager.TEnd 360 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 1
echo ""
echo "refinement level: 1"
echo ""
simdir=$outdir/linearsolver/2dumfpack1

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -TimeManager.TEnd 360 \
  -Grid.Cells0 '12 20 8' -Grid.Cells1 '5 50' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 2
echo ""
echo "refinement level: 2"
echo ""
simdir=$outdir/linearsolver/2dumfpack2

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -TimeManager.TEnd 360 \
  -Grid.Cells0 '24 40 16' -Grid.Cells1 '5 100' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 3
echo ""
echo "refinement level: 3"
echo ""
simdir=$outdir/linearsolver/2dumfpack3

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -TimeManager.TEnd 360 \
  -Grid.Cells0 '48 80 32' -Grid.Cells1 '5 200' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

# refinement level: 4
echo ""
echo "refinement level: 4"
echo ""
simdir=$outdir/linearsolver/2dumfpack4

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  echo "Output folder already exists, aborting"
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -TimeManager.TEnd 360 \
  -Grid.Cells0 '96 160 64' -Grid.Cells1 '5 400' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

exit 0
