// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters
 */
#ifndef DUMUX_VENTILATION_GALLERY_SPATIAL_PARAMS_HH
#define DUMUX_VENTILATION_GALLERY_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

namespace Dumux
{
template<class TypeTag>
class VentilationGallerySpatialParams;

namespace Properties
{
// Set the spatial parameters
SET_TYPE_PROP(DarcySpatialParams, SpatialParams,
              Dumux::VentilationGallerySpatialParams<TypeTag>);

// Set the material law parametrized by absolute saturations
SET_TYPE_PROP(DarcySpatialParams, MaterialLaw,
              EffToAbsLaw<RegularizedVanGenuchten<typename GET_PROP_TYPE(TypeTag, Scalar)>>);
}

/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Definition of the spatial parameters
 */
template<class TypeTag>
class VentilationGallerySpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    enum {
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

public:
    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    VentilationGallerySpatialParams(const GridView &gridView)
        : ParentType(gridView)
    {
        permeabilityCOx_ = 5e-20;
        permeabilityConcrete_ = 1e-18;
        porosityCOx_ = 0.15;
        porosityConcrete_ = 0.3;
        alphaBJ_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ);

        // residual saturations
        paramsCOx_.setSwr(0.4);
        paramsCOx_.setSnr(0);
        paramsConcrete_.setSwr(0.01);
        paramsConcrete_.setSnr(0);

        //parameters for the vanGenuchten law
        paramsCOx_.setVgAlpha(1/15e6);
        paramsCOx_.setVgn(1.49);
        paramsConcrete_.setVgAlpha(1/2e6);
        paramsConcrete_.setVgn(1.54);
    }

    /*!
     * \brief Function for defining the intrinsic (absolute) permeability.
     *
     * \return intrinsic (absolute) permeability
     * \param globalPos The position of the center of the element
     */
    const Scalar intrinsicPermeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (isConcrete(globalPos))
        {
            return permeabilityConcrete_;
        }
        return permeabilityCOx_;
    }

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const Scalar intrinsicPermeability(const Element &element,
                                       const FVElementGeometry &fvGeometry,
                                       const int scvIdx) const
    {
        return intrinsicPermeabilityAtPos(fvGeometry.subContVol[scvIdx].global);
    }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param globalPos The position of the center of the element
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (isConcrete(globalPos))
        {
            return porosityConcrete_;
        }
        return porosityCOx_;
    }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        return porosityAtPos(fvGeometry.subContVol[scvIdx].global);
    }

    /*!
     * \brief return the parameter object for the material law
     *
     * \param globalPos The position of the center of the element
     */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        if (isConcrete(globalPos))
        {
            return paramsConcrete_;
        }
        return paramsCOx_;
    }

    /*!
     * \brief return the parameter object for the material law
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const int scvIdx) const
    {
        return materialLawParamsAtPos(element.geometry().center());
    }

    /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidHeatCapacity(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    {
        return 1000;
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The position of the center of the element
     */
    Scalar solidDensityAtPos(const GlobalPosition& globalPos) const
    {
        return 2000;
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidDensity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return solidDensityAtPos(element.geometry().center());
    }

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     *
     * \param globalPos The position of the center of the element
     */
    Scalar solidThermalConductivityAtPos(const GlobalPosition& globalPos) const
    {
        return 10;
    }

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        return solidThermalConductivityAtPos(element.geometry().center());
    }

    /*!
     * \brief Evaluate the Beavers-Joseph coefficient at given position
     *
     * \param globalPos The global position
     *
     * \return Beavers-Joseph coefficient
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition &globalPos) const
    {
        return alphaBJ_;
    }

private:
    //! indicate whether the soil is concrete, otherwise it is COx
    bool isConcrete(const GlobalPosition &globalPos) const
    {
        return (globalPos[1] < 6);
    }

    Scalar permeabilityCOx_;
    Scalar permeabilityConcrete_;
    Scalar porosityCOx_;
    Scalar porosityConcrete_;
    Scalar alphaBJ_;
    MaterialLawParams paramsCOx_;
    MaterialLawParams paramsConcrete_;
};

} // end namespace Dumux

#endif // DUMUX_VENTILATION_GALLERY_SPATIAL_PARAMS_HH
