/** \file
  *  \ingroup StaggeredModel
  *
  * \brief This file contains different wall function approaches for
  *        the normal and tangential velocity component.
  *
  * The wall functions have to be called from the Dirichlet function of
  * the problem files.
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  */

#ifndef DUMUX_VELOCITY_WALLFUNCTIONS_HH
#define DUMUX_VELOCITY_WALLFUNCTIONS_HH

//! \todo should be replaced by common staggered grid properties
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproperties.hh>

namespace Dumux {

/**
  * \brief Returns wall functions values for the velocity components.
  *
  * This class contains different wall function approaches and has to be called
  * from the Dirichlet function of the problem files
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  *
  * \tparam TypeTag TypeTag of the problem
  */
template<class TypeTag>
class VelocityWallFunctions
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
  enum { dim = GridView::dimension };

  typedef typename GridView::template Codim<0>::Entity Element;

  //! \brief Constructor
  VelocityWallFunctions(const GridView& gv)
  : mapperElement(gv)
  {
    enableUnsymmetrizedVelocityGradient_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableUnsymmetrizedVelocityGradient);
    wallNormalAxis_.resize(mapperElement.size());
    storedVelocityGradientTensor_.resize(mapperElement.size());
    for (unsigned int i = 0; i < mapperElement.size(); ++i)
    {
      wallNormalAxis_[i] = 1;
      storedVelocityGradientTensor_[i] = 0.0;
      for (unsigned int j = 0; j < dim; ++j)
      {
        for (unsigned int k = 0; k < dim; ++k)
        {
          storedVelocityGradientTensor_[i][j][k] = 0.0;
        }
      }
    }
  }

  //! \brief Constant value for velocity
  double constantValueVelocity(const Element& e,
                               double velocityValue) const
  {
    return velocityValue;
  }

  //! \brief Beavers-Joseph slip-velocity condition
  double beaversJosephSlipVelocity(const Element& e,
                                   double permeability, double alphaBJ//, unsigned int normDim, unsigned int tangDim // this is required for 3D
                                   ) const
  {
    //! \todo only works for isotropic porous media and normals along domain axes
    unsigned int normDim = wallNormalAxis_[mapperElement.index(e)];
    unsigned int tangDim = 1 - normDim;

    if (enableUnsymmetrizedVelocityGradient_ == false)
    {
      return std::sqrt(permeability) / alphaBJ
             * (storedVelocityGradientTensor_[mapperElement.index(e)][normDim][tangDim]
                 + storedVelocityGradientTensor_[mapperElement.index(e)][tangDim][normDim]);
    }
    else // enableUnsymmetrizedVelocityGradient_
    {
      return std::sqrt(permeability) / alphaBJ
             * storedVelocityGradientTensor_[mapperElement.index(e)][tangDim][normDim];
    }
  }

  MapperElement mapperElement;
  mutable std::vector<unsigned int> wallNormalAxis_;
  mutable std::vector<Dune::FieldMatrix<double, dim, dim> > storedVelocityGradientTensor_;

private:
  bool enableUnsymmetrizedVelocityGradient_;
};


} // end namespace Dumux

#endif // DUMUX_VELOCITY_WALLFUNCTIONS_HH
