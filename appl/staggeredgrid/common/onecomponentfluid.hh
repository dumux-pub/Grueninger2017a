// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Functions required by the Navier-Stokes staggered grid local operator
 *        for a fluid with only one component.
 */
#ifndef DUMUX_ONE_COMPONENT_FLUID_HH
#define DUMUX_ONE_COMPONENT_FLUID_HH

// TODO: Replace the onecomponentfluid.hh with functionality form dumux-stable

//! \todo should be replaced by common staggered grid properties
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproperties.hh>

namespace Dumux
{

/*!
 * \brief Functions required by the Navier-Stokes staggered grid local operator
 *        for a fluid with only one component.
 */
template <class TypeTag>
class OneComponentFluid
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
  typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

  /**
    * \brief Returns the density [kg/m^3]
    */
  static const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    return FluidSystem::density(fluidState, phaseIdx);
  }

  /**
    * \brief Returns the dynamic viscosity [kg/(m s)]
    */
  static const Scalar dynamicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    return FluidSystem::viscosity(fluidState, phaseIdx);
  }

  /**
    * \brief Returns the kinematic viscosity [m^2/s]
    */
  static const Scalar kinematicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    return FluidSystem::viscosity(fluidState, phaseIdx)
           / FluidSystem::density(fluidState, phaseIdx);
  }
};
} // end namespace

#endif // DUMUX_ONE_COMPONENT_FLUID_HH
