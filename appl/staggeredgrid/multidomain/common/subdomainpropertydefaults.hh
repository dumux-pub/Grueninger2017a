// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup MultidomainModel
 * \brief Specify default properties required in the subdomains of dune-multidomain
 */
#ifndef DUMUX_SUBDOMAIN_STAGGERED_PROPERTYDEFAULTS_HH
#define DUMUX_SUBDOMAIN_STAGGERED_PROPERTYDEFAULTS_HH

#include <dune/pdelab/backend/istl/utility.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>

#include <appl/staggeredgrid/localfunctions/staggeredq0fem.hh>

#include "multidomainnewtonmethod.hh"
#include "multidomainpropertydefaults.hh"
#include "subdomainproperties.hh"

namespace Dumux
{

namespace Properties
{

SET_PROP(SubDomainStaggered, Grid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
public:
    typedef typename GET_PROP_TYPE(MultiDomain, MultiDomainGrid) type;
};

// grid view type
SET_PROP(SubDomainStaggered, GridView)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
public:
    typedef typename GET_PROP_TYPE(MultiDomain, MultiDomainGridView) type;
};
SET_PROP(SubDomainStaggered, SubDomainGridView)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
public:
    typedef typename GET_PROP_TYPE(MultiDomain, SubDomainGridView) type;
};

// finite element maps
SET_PROP(SubDomainStaggered, FiniteElementMapP0)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
    typedef typename GET_PROP_TYPE(MultiDomain, Scalar) Scalar;
    using MDGV = typename GET_PROP_TYPE(MultiDomain, MultiDomainGridView);
    enum { dim = MDGV::dimension };
public:
    typedef Dune::PDELab::P0LocalFiniteElementMap<Scalar, Scalar, dim> type;
};
SET_PROP(SubDomainStaggered, FiniteElementMapStaggered)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
    typedef typename GET_PROP_TYPE(MultiDomain, Scalar) Scalar;
    using MDGV = typename GET_PROP_TYPE(MultiDomain, MultiDomainGridView);
    enum { dim = MDGV::dimension };
public:
    typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<Scalar, Scalar, dim> type;
};

// Set property value for the DimVector
SET_PROP(SubDomainStaggered, DimVector)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
public:
    typedef typename GET_PROP_TYPE(MultiDomain, DimVector) type;
};

// Set property value for the time manager
SET_PROP(SubDomainStaggered, TimeManager)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomain;
public:
    typedef typename GET_PROP_TYPE(MultiDomain, TimeManager) type;
};

// modified Newton method
SET_TYPE_PROP(SubDomainStaggered, NewtonMethod, PDELabNewtonMethod<TypeTag>);

// set base local residual calculation (mainly for darcy)
SET_TYPE_PROP(SubDomainStaggered, BaseLocalResidual, MultidomainCCLocalResidual<TypeTag>);
} // namespace Properties
} // namespace Dumux

#endif // DUMUX_SUBDOMAIN_STAGGERED_PROPERTYDEFAULTS_HH
