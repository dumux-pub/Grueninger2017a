// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \brief Special case to filter out multidomain corner cases.
 */
#ifndef DUMUX_MULTIDOMAIN_CC_LOCALRESIDUAL_HH
#define DUMUX_MULTIDOMAIN_CC_LOCALRESIDUAL_HH

#include <dumux/implicit/cellcentered/localresidual.hh>

namespace Dumux
{
/*!
 * \brief Special case to filter out multidomain corner cases.
 */
template<class TypeTag>
class MultidomainCCLocalResidual
: public CCLocalResidual<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;

    // copying the local residual class is not a good idea
    MultidomainCCLocalResidual(const MultidomainCCLocalResidual&);

public:
    /*!
     * \brief Constructor. Calls constructor of father class.
     */
    MultidomainCCLocalResidual()
    : CCLocalResidual<TypeTag>()
    {}

    /*!
     * \brief Add the flux terms to the local residual of the current element
     *
     * This is a copy of evalFluxes_() from the father class. It prevents
     * calculating fluxes across different subdomains.
     */
    void evalFluxes_()
    {
        // calculate the mass flux over the faces and subtract
        // it from the local rates
        int fIdx = -1;
        IntersectionIterator isIt = this->gridView_().ibegin(this->element_());
        IntersectionIterator isEndIt = this->gridView_().iend(this->element_());
        for (; isIt != isEndIt; ++isIt)
        {
            // only consider neighbors and intersection between elements of the same SubDomain
            if (isIt->neighbor()
                && (this->gridView_().indexSet().contains(1, isIt->inside())
                    && this->gridView_().indexSet().contains(1, isIt->outside()))
               )
            {
                fIdx++;
                PrimaryVariables flux;

                Valgrind::SetUndefined(flux);
                this->asImp_()->computeFlux(flux, fIdx);
                Valgrind::CheckDefined(flux);

                flux *= this->curVolVars_(0).extrusionFactor();

                this->residual_[0] += flux;
            }
        }
    }

 private:
    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }

    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }
};

} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_CC_LOCALRESIDUAL_HH
