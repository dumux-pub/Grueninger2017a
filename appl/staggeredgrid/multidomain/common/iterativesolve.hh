#include <dune/istl/superlu.hh>

#define SUPERLU_MALLOC(x) malloc(x)
#define SUPERLU_FREE(x) free(x)

extern "C" {
#include "/home/lh2/gruenich/downloads/superlu/CBLAS/daxpy.c"
#include "/home/lh2/gruenich/downloads/superlu/CBLAS/dcopy.c"
#include "dfgmr.c"
}
#undef abs
#undef max
#undef min

superlu_options_t *GLOBAL_OPTIONS;
double *GLOBAL_R, *GLOBAL_C;
int *GLOBAL_PERM_C, *GLOBAL_PERM_R;
SuperMatrix *GLOBAL_A, *GLOBAL_L, *GLOBAL_U;
SuperLUStat_t *GLOBAL_STAT;
mem_usage_t   *GLOBAL_MEM_USAGE;

void dpsolve(int n,
             double x[], /* solution */
             double y[]  /* right-hand side */
)
{
    SuperMatrix *A = GLOBAL_A, *L = GLOBAL_L, *U = GLOBAL_U;
    SuperLUStat_t *stat = GLOBAL_STAT;
    int *perm_c = GLOBAL_PERM_C, *perm_r = GLOBAL_PERM_R;
    char equed[1] = {'N'};
    double *R = GLOBAL_R, *C = GLOBAL_C;
    superlu_options_t *options = GLOBAL_OPTIONS;
    mem_usage_t  *mem_usage = GLOBAL_MEM_USAGE;
    int info;
    static DNformat X, Y;
    static SuperMatrix XX = {SLU_DN, SLU_D, SLU_GE, 1, 1, &X};
    static SuperMatrix YY = {SLU_DN, SLU_D, SLU_GE, 1, 1, &Y};
    double rpg, rcond;

    XX.nrow = YY.nrow = n;
    X.lda = Y.lda = n;
    X.nzval = x;
    Y.nzval = y;

    dgsisx(options, A, perm_c, perm_r, NULL, equed, R, C,
       L, U, NULL, 0, &YY, &XX, &rpg, &rcond, NULL,
       mem_usage, stat, &info);
}

void dmatvec_mult(double alpha, double x[], double beta, double y[])
{
    SuperMatrix *A = GLOBAL_A;

    sp_dgemv((char*)"N", alpha, A, x, 1, beta, y, 1);
}

/**
 * solve linesr system Ax = b
 * Scalar: field type
 * a_:     matrix
 * x_:     guess solution, used as initial solution for iterative solver and will contain solution
 * b_:     right hand side
 */
template<typename Scalar>
void preconditionedGmres(const Dune::BCRSMatrix<Dune::FieldMatrix<Scalar,1,1>>& a_,
                         Dune::BlockVector<Dune::FieldVector<Scalar,1>>& x_,
                         const Dune::BlockVector<Dune::FieldVector<Scalar,1>>& b_,
                         unsigned int restart = 100, unsigned int maxIterations = 1000,
                         Scalar tolerance = 1e-8, unsigned int verbosity = 1)
{
    // prepare matrix, use Dune classes
    using Matrix = Dune::BCRSMatrix<Dune::FieldMatrix<Scalar,1,1>>;
    using SuperLUMatrix = Dune::SuperLUMatrix<Matrix>;
    SuperLUMatrix a;
    a = a_;
    unsigned int size = a_.N();

    // prepare right hand side and solution container
    SuperMatrix b, x;
    Scalar* dataB = (Scalar*) malloc(size * sizeof(Scalar));
    Scalar* dataX = (Scalar*) malloc(size * sizeof(Scalar));
    dCreate_Dense_Matrix(&b, size, 1, dataB, size, SLU_DN, SLU_D, SLU_GE);
    dCreate_Dense_Matrix(&x, size, 1, dataX, size, SLU_DN, SLU_D, SLU_GE);
    for (unsigned int i = 0; i < size; ++i)
    {
      dataB[i] = b_[i];
      dataX[i] = x_[i];
    }

    // prepare all other variables for ILU computation
    superlu_options_t options;
    ilu_set_default_options(&options);
    options.PivotGrowth = YES;
    options.ConditionNumber = YES;
    int* permC = intMalloc(size);
    int* permR = intMalloc(size);
    int* etree = intMalloc(size);
    char equed[1] = {'B'};
    Scalar* r = (Scalar*) superlu_malloc(size * sizeof(Scalar));
    Scalar* c = (Scalar*) superlu_malloc(size * sizeof(Scalar));
    SuperMatrix l, u;
    Scalar *work = NULL;
    int lwork = 0;
    Scalar rpg, rcond;
    GlobalLU_t glu;
    mem_usage_t mem_usage;
    SuperLUStat_t stat;
    StatInit(&stat);
    int info = 0;

    // compute ILU using dgsisx from SuperLU package
    b.ncol = 0;  // no triangular solution
    dgsisx(&options, &static_cast<SuperMatrix&>(a), permC, permR, etree, equed, r, c, &l, &u,
           work, lwork, &b, &x, &rpg, &rcond, &glu, &mem_usage, &stat, &info);

    // right-hand side for iterative solver
    Scalar* bIter = (Scalar*) malloc(size * sizeof(Scalar));
    Scalar* xIter = (Scalar*) malloc(size * sizeof(Scalar));
    if (*equed == 'R' || *equed == 'B')
    {
        for (unsigned int i = 0; i < size; ++i)
        {
            bIter[i] = dataB[i] * r[i];
            xIter[i] = dataX[i] * r[i];
        }
    }
    else
    {
        for (unsigned int i = 0; i < size; ++i)
        {
            bIter[i] = dataB[i];
            xIter[i] = dataX[i];
        }
    }

    if (verbosity > 2)
    {
        std::cout << "      dgsisx(): info " << info << ", equed " << equed[0] << std::endl;
    }
    if (verbosity > 4 && (info > 0 || rcond < 1e-8 || rpg > 1e8))
    {
        std::cout << "      WARNING: This preconditioner might be unstable." << std::endl;
    }

    if (verbosity > 1)
    {
        if (info == 0 || info == size + 1)
        {
            if (options.PivotGrowth == YES)
            {
                std::cout << "      Recip. pivot growth = " << rpg << std::endl;
            }
            if (options.ConditionNumber == YES)
            {
                std::cout << "      Recip. condition number = " << rcond << std::endl;
            }
        }
        else if ( info > 0 && lwork == -1 )
        {
            std::cout << "      Estimated memory: " << (info - size) << " bytes" << std::endl;
        }
    }

    NCformat* aStore = (NCformat*) static_cast<SuperMatrix&>(a).Store;
    SCformat* lStore = (SCformat*) l.Store;
    NCformat* uStore = (NCformat*) u.Store;
    if (verbosity > 2)
    {
        std::cout << "      n(A) = " << size << ", nnz(A) = " << aStore->nnz << std::endl;
        std::cout << "      Number of nonzeros in factor L = " << lStore->nnz << std::endl;
        std::cout << "      Number of nonzeros in factor U = " << uStore->nnz << std::endl;
        std::cout << "      Number of nonzeros in L+U = " << (lStore->nnz + uStore->nnz - size) << std::endl;
        std::cout << "      Fill ratio: nnz(F)/nnz(A) = " <<
            (((Scalar)(lStore->nnz + uStore->nnz - size))
            / ((Scalar)aStore->nnz)) << std::endl;
        std::cout << "      L\\U MB " << (mem_usage.for_lu/1e6) << " total MB needed " << (mem_usage.total_needed/1e6) << std::endl;
    }

    // assign to global variables
    GLOBAL_A = &static_cast<SuperMatrix&>(a);
    GLOBAL_L = &l;
    GLOBAL_U = &u;
    GLOBAL_STAT = &stat;
    GLOBAL_PERM_C = permC;
    GLOBAL_PERM_R = permR;
    GLOBAL_OPTIONS = &options;
    GLOBAL_R = r;
    GLOBAL_C = c;
    GLOBAL_MEM_USAGE = &mem_usage;

    // options: solve-only
    options.Fact = FACTORED;
    options.PivotGrowth = NO;
    options.ConditionNumber = NO;

    // set values for GMRES
    int iter = maxIterations;

    if (info <= size + 1)
    {
        // GMRES
        unsigned int result =
            dfgmr(size, dmatvec_mult, dpsolve, bIter, xIter, tolerance, restart, &iter, verbosity > 3 ? stdout : NULL);

        if (verbosity > 0)
        {
          std::cout << "      Number of linear solver steps: " << iter << std::endl;
        }

        if (result == 1)
        {
          // free now unused variables
          SUPERLU_FREE (dataB);
          SUPERLU_FREE (dataX);
          SUPERLU_FREE (etree);
          SUPERLU_FREE (permR);
          SUPERLU_FREE (permC);
          SUPERLU_FREE (r);
          SUPERLU_FREE (c);
          Destroy_SuperMatrix_Store(&b);
          Destroy_SuperMatrix_Store(&x);
          if (lwork >= 0)
          {
              Destroy_SuperNode_Matrix(&l);
              Destroy_CompCol_Matrix(&u);
          }
          SUPERLU_FREE(bIter);
          SUPERLU_FREE(xIter);
          StatFree(&stat);

          DUNE_THROW(Dumux::NumericalProblem, "Linear solver did not converge.");
        }

        // scale the solution back if equilibration was performed
        if (*equed == 'C' || *equed == 'B')
        {
            for (unsigned int i = 0; i < size; ++i)
            {
                xIter[i] *= c[i];
            }
        }
    }

    // copy solution to Dune vector
    for (unsigned int i = 0; i < size; ++i)
    {
        x_[i] = xIter[i];
    }

    // free now unused variables
    SUPERLU_FREE (dataB);
    SUPERLU_FREE (dataX);
    SUPERLU_FREE (etree);
    SUPERLU_FREE (permR);
    SUPERLU_FREE (permC);
    SUPERLU_FREE (r);
    SUPERLU_FREE (c);
    Destroy_SuperMatrix_Store(&b);
    Destroy_SuperMatrix_Store(&x);
    if (lwork >= 0)
    {
        Destroy_SuperNode_Matrix(&l);
        Destroy_CompCol_Matrix(&u);
    }
    SUPERLU_FREE(bIter);
    SUPERLU_FREE(xIter);
    StatFree(&stat);
}
