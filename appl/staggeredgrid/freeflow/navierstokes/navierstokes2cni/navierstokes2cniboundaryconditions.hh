// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for Navier-Stokes compositional non-isothermal boundary conditions
 */
#ifndef DUMUX_NAVIERSTOKES_BOUNDARY_TWOCNI_CONDITIONS_HH
#define DUMUX_NAVIERSTOKES_BOUNDARY_TWOCNI_CONDITIONS_HH

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

#include "navierstokes2cniproperties.hh"
#include "navierstokes2cnipropertydefaults.hh"
#include "../navierstokes/navierstokesboundaryconditions.hh"

namespace Dumux
{
/**
 * \brief Boundary condition function for the component massMoleFrac.
 */
template<class TypeTag>
class BCMassMoleFrac
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCMassMoleFrac (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Wall Boundary for massMoleFrac
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcMassMoleFracIsWall(global);
  }

  //! \brief Return whether Intersection is Inflow Boundary for massMoleFrac
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcMassMoleFracIsInflow(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for massMoleFrac
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcMassMoleFracIsOutflow(global);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for massMoleFrac
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcMassMoleFracIsSymmetry(global);
  }

  //! \brief Return whether Intersection is Coupling Boundary for massMoleFrac
  //! \tparam I Intersection type
  template<typename I>
  bool isCoupling(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcMassMoleFracIsCoupling(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Boundary condition function for the component temperature.
 */
template<class TypeTag>
class BCTemperature
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCTemperature (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Wall Boundary for temperature
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTemperatureIsWall(global);
  }

  //! \brief Return whether Intersection is Inflow Boundary for temperature
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTemperatureIsInflow(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for temperature
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTemperatureIsOutflow(global);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for temperature
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTemperatureIsSymmetry(global);
  }

  //! \brief Return whether Intersection is Coupling Boundary for temperature
  //! \tparam I Intersection type
  template<typename I>
  bool isCoupling(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTemperatureIsCoupling(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for massMoleFrac Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletMassMoleFrac
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    DirichletMassMoleFrac<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletMassMoleFrac<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  DirichletMassMoleFrac (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().dirichletMassMoleFracAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for temperature Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletTemperature
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    DirichletTemperature<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletTemperature<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  DirichletTemperature (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().dirichletTemperatureAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Function for massMoleFrac Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannMassMoleFrac
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    NeumannMassMoleFrac<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannMassMoleFrac<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannMassMoleFrac (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannMassMoleFracAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for temperature Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannTemperature
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    NeumannTemperature<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannTemperature<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannTemperature (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannTemperatureAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Source term function for the component balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceComponentBalance
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    SourceComponentBalance<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceComponentBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceComponentBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceComponentBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Source term function for the energy balance.
 *
 * \tparam GV GridView type
 * \tparam RF Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceEnergyBalance
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    SourceEnergyBalance<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceEnergyBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceEnergyBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceEnergyBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


} // end namespace Dumux

#endif
